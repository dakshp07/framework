<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class SearchController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'ElasticSearch'
        $this->loadComponent('ElasticSearch');

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * beforeFilter method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->session = $this->getRequest()->getSession();

        // Get search Settings for Session
        $this->searchSettings = $this->session->read('searchSettings');

        $this->searchCategory = [
            // 'keyword',
            'publication',
            'collection',
            'provenience',
            'period',
            'inscription',
            // 'id'
        ];

        // Search Result View Settings
        $this->settings = [
            'LayoutType' => 1,
            'Page' => 1,
            'PageSize' => $this->searchSettings['PageSize'],
            'lastPage' => 0,
            'canViewPrivateArtifacts' => $this->GeneralFunctions->checkIfRolesExists([1, 4]) == 1 ? 1 : 0,
            'canViewPrivateInscriptions' => $this->GeneralFunctions->checkIfRolesExists([1, 5]) == 1 ? 1 : 0,
            // 'sortBy' => 'relevance',
            'filter_dirty' => 0
        ];

        $this->filters = [
            'collection' => [],
            'period' => [],
            'provenience' => [],
            'atype' => [],
            'materials' => [],
            'authors' => [],
            'year' => []
        ];

        // To store whole array result with key as searchId (as timestamp) in session vaiable
        // Max searchId to be store = 4
        if (is_null($this->session->read('resultsStored'))) {
            $this->session->write('resultsStored', []);
            $this->resultsStored = [];
        } else {
            $this->resultsStored = $this->session->read('resultsStored');
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        $queryData = $this->request->params['?'];

        if (!empty($queryData)) {
            $searchId = time();

            return $this->redirect([
                'action' => 'view',
                $searchId,
                '?' => $queryData
            ]);
        } else {
            return $this->redirect($this->referer());
        }
    }

    /**
     * filter method
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function filter($searchId)
    {
        // Filter submitted through form
        $filterData = $this->request->data;

        $searchIdArray = explode('-', $searchId);

        if ($searchIdArray[0] === 'reset') {
            $searchId = $searchIdArray[1];
            $currentFilters = $this->resultsStored[$searchId]['filters'];

            $toBeReset = [];

            foreach ($currentFilters as $filter => $values) {
                $toBeReset[$filter] = array_keys(array_filter(
                    $values,
                    function ($value) {
                        return $value;
                    }
                ));
            }

            foreach ($toBeReset as $filter => $values) {
                foreach ($values as $value) {
                    $currentFilters[$filter][$value] = 0;
                }
            }
        } else {
            // Get filters stored in session
            $currentFilters = $this->resultsStored[$searchId]['filters'];

            $currentAppliedFilters = [];

            // Extract filters values which are set
            foreach ($currentFilters as $filter => $values) {
                // Check if filter values are selected or not
                if (!empty($values)) {
                    $selectedValues = array_keys(array_filter(
                        $values,
                        function ($value) {
                            return $value;
                        }
                    ));

                    // If empty check if the $filter exists in $filterData
                    if (!empty($selectedValues)) {
                        $currentAppliedFilters[$filter] = $selectedValues;
                    } else {
                        if (array_key_exists($filter, $filterData)) {
                            $currentAppliedFilters[$filter] = [];
                        }
                    }
                } else {
                    // If empty check if the $filter exists in $filterData
                    if (array_key_exists($filter, $filterData)) {
                        $currentAppliedFilters[$filter] = [];
                    }
                }
            }

            // Check if filters are updated
            $checkIfFilterChanged = 0;

            // Calculate difference between requested and store filters
            foreach ($currentAppliedFilters as $filter => $value) {
                $newValue = $filterData[$filter];
                $newValue = $newValue == '' ? [] : $newValue;

                $arrayDiffRemoved = array_diff($value, $newValue);
                $arrayDiffAdded = array_diff($newValue, $value);
                
                // If there is change in requested filter applied
                if (!empty($arrayDiffAdded) || !empty($arrayDiffRemoved)) {
                    $checkIfFilterChanged = 1;
                    
                    // If unselected
                    foreach (array_diff($value, $newValue) as $changedValue) {
                        $currentFilters[$filter][$changedValue] = 0;
                    }
                    
                    // If selected
                    foreach (array_diff($newValue, $value) as $changedValue) {
                        $currentFilters[$filter][$changedValue] = 1;
                    }
                }
            }

            // If there is no filter change then redirect to previous page
            if (!$checkIfFilterChanged) {
                $this->redirect($this->referer());
            }
        }

        // Set filter_dirty
        $this->session->write('resultsStored.'.$searchId.'.settings.filter_dirty', 1);

        // Set new filter
        $this->session->write('resultsStored.'.$searchId.'.filters', $currentFilters);

        $queryData = [];

        foreach ($this->request->query as $param => $values) {
            $status = in_array($param, $this->searchCategory) || $param == 'LayoutType';
            if ($status) {
                $queryData = array_merge($queryData, [$param => $values]);
            }
        }

        $params = [];
        $params = array_merge($params, $queryData);

        return $this->redirect([
            'action' => 'view',
            $searchId,
            '?' => $params
        ]);
    }

    /**
     * view method
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function view($searchId)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        if (substr($searchId, 0, 6) === 'filter') {
            return $this->setaction('filter', substr($searchId, 7));
        } else {
            $queryData = $this->optimizedQuery($this->request->params['?']);

            $resultSet = empty($this->resultsStored) ? -1 : (array_key_exists($searchId, $this->resultsStored) ? $this->resultsStored[$searchId]['resultSet'] : null);

            // To check if redirected from filter function
            $filterDirtyStatus = empty($this->resultsStored) ? 0 : $this->session->read('resultsStored.'.$searchId.'.settings.filter_dirty');
            
            if ($filterDirtyStatus) {
                $this->settings['Page'] = 1;
            }

            $requestedResultSet = (int)floor(($this->settings['Page'] - 1) * $this->settings['PageSize']/ 10000);
        
            // To check if requested page is within the resultSet.
            $outOfBoundPage = 0;

            if (!is_null($resultSet) && $requestedResultSet != $resultSet) {
                $outOfBoundPage = 1;
            }

            if (!array_key_exists($searchId, $this->resultsStored) || $outOfBoundPage || $filterDirtyStatus) {
                $this->getSearchResults($queryData, $searchId);
            }

            if ($requestedResultSet > 0) {
                $offset = ($this->settings['Page'] - 1) * $this->settings['PageSize'] - $requestedResultSet * 10000;
            } else {
                $offset = ($this->settings['Page'] == 1) ? 0 : ($this->settings['Page'] - 1) * $this->settings['PageSize'];
            }

            $result = array_slice($this->resultsStored[$searchId]['result'], $offset, $this->settings['PageSize'], true);

            $this->settings['lastPage'] = (int)ceil($this->resultsStored[$searchId]['totalHits']/$this->settings['PageSize']);

            foreach ($result as $artifactID => $values) {
                $result[$artifactID] = array_merge(
                    $result[$artifactID],
                    $this->ElasticSearch->getPublicationWithArtifactId($artifactID)
                );

                $result[$artifactID]['inscription'] = $this->ElasticSearch->getInscriptionWithArtifactId($artifactID, $this->settings['canViewPrivateInscriptions']);
            }

            $this->set([
                'searchId' => $searchId,
                'result' => $result,
                'LayoutType' => $this->settings['LayoutType'],
                'PageSize' => $this->settings['PageSize'],
                'Page' => $this->settings['Page'],
                'lastPage' => $this->settings['lastPage'],
                'searchSettings' => $this->searchSettings,
                'filters' => $this->resultsStored[$searchId]['filters'],
                'searchableFields' => $this->searchCategory
            ]);
        }
    }

    /**
     * getDataFromRequest method
     *
     *  It returns array of required Key=>Value Pair.
     *
     * @param
     * param : Type of Request i.e. data(URL params) or query(form POST) parameters.
     *
     * @return Array of Key=>Value pair.
     */
    public function getDataFromRequest($requestType)
    {
        $queryData = [];

        foreach ($requestType as $key => $value) {
            if ($value !== '') {
                if (array_key_exists($key, $this->settings)) {
                    $this->settings[$key] = $value;
                } elseif (in_array($key, $this->searchableFields)) {
                    $queryData[$key] = trim($value, '" || \'');
                }
            }
        }

        return $queryData;
    }

    /**
     * optimizedQuery method
     *
     * Handles AND/OR condition for same search Category.
     *
     * @param
     * param : URL Parameters.
     *
     * @return \Cake\Http\Response|void
     */
    public function optimizedQuery($param)
    {
        $paramModifiedArray = [];

        foreach ($param as $key => $value) {
            $checkType = substr($key, 0, -1);

            if (in_array($key, $this->searchCategory)) {
                array_push($paramModifiedArray, [$key => trim($value, '" || \'')]);
            } elseif (in_array($checkType, $this->searchCategory) || $checkType === 'operator') {
                array_push($paramModifiedArray, [$checkType => trim($value, '" || \'')]);
            } elseif (array_key_exists($key, $this->settings)) {
                $this->settings[$key] = $value;
            }
        }

        $sizeOfParamModifiedArray = sizeof($paramModifiedArray);
        
        $finalQueryArray = [];
        $trackingField = [];

        for ($loop = 0; $loop < $sizeOfParamModifiedArray; $loop++) {
            $key = array_keys($paramModifiedArray[$loop])[0];
            $value = array_values($paramModifiedArray[$loop])[0];

            if ($key === 'operator' && ($loop + 1) < $sizeOfParamModifiedArray) {
                $nextKey = array_keys($paramModifiedArray[$loop + 1])[0];
                $nextValue = array_values($paramModifiedArray[$loop+1])[0];

                if (key_exists($nextKey, $trackingField)) {
                    $previousPosition = $trackingField[$nextKey];
                    $getPrevValueFromFinalQueryArray = array_values($finalQueryArray[$previousPosition])[0];

                    $finalQueryArray[$previousPosition][$nextKey] =  $getPrevValueFromFinalQueryArray.' '.$value.' '.$nextValue;
                    $loop += 1;
                } else {
                    array_push($finalQueryArray, $paramModifiedArray[$loop]);
                }
            } else {
                array_push($finalQueryArray, $paramModifiedArray[$loop]);
                $trackingField[$key] =  sizeof($finalQueryArray) - 1;
            }
        }

        return $finalQueryArray;
    }

    /**
     * getFilterDataFromSession method
     *
     * Get Filters stored in Session for specific searchId.
     *
     * @param
     * searchId : Unique Search ID.
     *
     * @return Array of Filters
     */
    public function getFilterDataFromSession($searchId)
    {
        $currentFilters = $this->getRequest()->getSession()->read('resultsStored.'.$searchId.'.filters');
        
        $currentFilters = is_null($currentFilters) ? [] : $currentFilters;

        $setFilters = [];

        foreach ($currentFilters as $filter => $values) {
            if (!empty($values)) {
                $selectedValues = array_keys(array_filter(
                    $values,
                    function ($value) {
                        return $value;
                    }
                ));

                if (!empty($selectedValues)) {
                    $setFilters[$filter] = $selectedValues;
                }
            }
        }

        return $setFilters;
    }

    /**
     * getSearchResults method
     *
     * @param
     * queryData : Array of [Key => Value] where Key is searchCategory and value is value of searchCategory.
     * searchId : Unique Search ID.
     *
     * @return \Cake\Http\Response|void
     */
    public function getSearchResults($queryData, $searchId)
    {
        // If there are 4 search results already stored in session.
        if (sizeof($this->resultsStored) == 4) {
            $firstInsertedKey = min(array_keys($this->resultsStored));
            if ($searchId != $firstInsertedKey) {
                unset($this->resultsStored[$firstInsertedKey]);
            }
        }

        $filterData = $this->getFilterDataFromSession($searchId);

        // Retrieve results and updated settings from ElasticSearch Components
        $resultFromESComponent = $this->ElasticSearch->simpleSearch($queryData, $this->settings, $filterData);

        if ($this->session->read('resultsStored.'.$searchId.'.settings.filter_dirty') || !empty($filterData)) {
            $resultFromESComponent['filters'] = $this->session->read('resultsStored.'.$searchId.'.filters');
        }

        $resultFromESComponent['settings'] = $this->settings;
        
        $this->resultsStored[$searchId] = $resultFromESComponent;
        
        // Store in Session variables
        $this->session->write('resultsStored', $this->resultsStored);
    }
}
