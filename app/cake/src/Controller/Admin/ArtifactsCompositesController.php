<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactsComposites Controller
 *
 * @property \App\Model\Table\ArtifactsCompositesTable $ArtifactsComposites
 *
 * @method \App\Model\Entity\ArtifactsComposite[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsCompositesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts']
        ];
        $artifactsComposites = $this->paginate($this->ArtifactsComposites);

        $this->set(compact('artifactsComposites'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Composite id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsComposite = $this->ArtifactsComposites->get($id, [
            'contain' => ['Artifacts']
        ]);

        $this->set('artifactsComposite', $artifactsComposite);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsComposite = $this->ArtifactsComposites->newEntity();
        if ($this->request->is('post')) {
            $artifactsComposite = $this->ArtifactsComposites->patchEntity($artifactsComposite, $this->request->getData());
            if ($this->ArtifactsComposites->save($artifactsComposite)) {
                $this->Flash->success(__('The artifacts composite has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts composite could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsComposites->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('artifactsComposite', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Composite id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsComposite = $this->ArtifactsComposites->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsComposite = $this->ArtifactsComposites->patchEntity($artifactsComposite, $this->request->getData());
            if ($this->ArtifactsComposites->save($artifactsComposite)) {
                $this->Flash->success(__('The artifacts composite has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts composite could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsComposites->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('artifactsComposite', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Composite id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsComposite = $this->ArtifactsComposites->get($id);
        if ($this->ArtifactsComposites->delete($artifactsComposite)) {
            $this->Flash->success(__('The artifacts composite has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts composite could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
