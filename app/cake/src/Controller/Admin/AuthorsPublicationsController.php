<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class AuthorsPublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'order' => [
                'author_id' => 'ASC',
                'publication_id' => 'ASC'
            ],
            'contain' => ['Publications', 'Authors']
        ];
        $authorsPublications = $this->paginate($this->AuthorsPublications);

        $this->set(compact('authorsPublications'));
    }

    /**
     * Add method.
     *
     * @param string $flag type of add operation.
     * '' => Normal add,
     * 'author' => Add author/editor link to a selected publication,
     * 'publication' => Add link to a selected author,
     * 'bulk' => Bulk add links.
     * @param int $id Id of the selected publication or selected author/editor for $flag = 'author' and $flag = 'publication' respectively.
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = '', $id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->loadModel('EditorsPublications');
        if ($flag == '') {
            $authors_list = $this->AuthorsPublications->Authors->find('all');
            $authors_mapping = [];
            foreach ($authors_list as $row) {
                $authors_mapping[$row['author']] = $row['id'];
            }
            $authors_names = array_keys($authors_mapping);
            $authorsPublication = $this->AuthorsPublications->newEntity();
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                if (in_array($data['author_id'], $authors_names)) {
                    $data['author_id'] = $authors_mapping[$data['author_id']];
                } else {
                    $data['author_id'] = 0;
                }
                $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $data);
                if ($this->AuthorsPublications->save($authorsPublication)) {
                    $this->Flash->success(__('New link has been saved.'));
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('The link could not be saved. Please, try again.'));
                }
            }
            $this->set(compact('authorsPublication', 'authors_names', 'flag'));
        } elseif ($flag == 'author') {
            $this->paginate = [
                'AuthorsPublications' => ['limit' => 10, 'scope' => 'authorspublications'],
                'EditorsPublications' => ['limit' => 10, 'scope' => 'editorspublications']
            ];
            $authorsPublication = $this->AuthorsPublications->newEntity();
            $editorsPublication = $this->EditorsPublications->newEntity();
            $authors_list = $this->AuthorsPublications->Authors->find('all');
            $authors_mapping = [];
            foreach ($authors_list as $row) {
                $authors_mapping[$row['author']] = $row['id'];
            }
            $authors_names = array_keys($authors_mapping);
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                if (array_key_exists('author_id', $data)) {
                    // For adding author publication link
                    if (in_array($data['author_id'], $authors_names)) {
                        $data['author_id'] = $authors_mapping[$data['author_id']];
                    } else {
                        $data['author_id'] = 0;
                    }
                    $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $data);
                    if ($this->AuthorsPublications->save($authorsPublication)) {
                        $this->Flash->success(__('The author has been linked.'));
                        return $this->redirect(['action' => 'add', $flag, $id]);
                    }
                    $this->Flash->error(__('The author could not be linked. Please, try again.'));
                } else {
                    // For adding editor publication link
                    if (in_array($data['editor_id'], $authors_names)) {
                        $data['editor_id'] = $authors_mapping[$data['editor_id']];
                    } else {
                        $data['editor_id'] = 0;
                    }
                    $editorsPublication = $this->EditorsPublications->patchEntity($editorsPublication, $data);
                    if ($this->EditorsPublications->save($editorsPublication)) {
                        $this->Flash->success(__('The editor has been linked.'));
                        return $this->redirect(['action' => 'add', $flag, $id]);
                    }
                    $this->Flash->error(__('The editor could not be linked. Please, try again.'));
                }
            }
            $authorsPublications = $this->paginate($this->AuthorsPublications->find('all', ['order' => 'author_id', 'contain' => ['Authors', 'Publications']])->where(['publication_id' => $id]));
            $editorsPublications = $this->paginate($this->EditorsPublications->find('all', ['order' => 'editor_id', 'contain' => ['Authors', 'Publications']])->where(['publication_id' => $id]));
            $this->set(compact('id', 'authorsPublications', 'authorsPublication', 'editorsPublications', 'editorsPublication', 'authors_names', 'flag'));
        } elseif ($flag == 'publication') {
            $this->paginate = [
                'AuthorsPublications' => ['limit' => 10, 'scope' => 'authorspublications'],
                'EditorsPublications' => ['limit' => 10, 'scope' => 'editorspublications']
            ];
            $authorsPublication = $this->AuthorsPublications->newEntity();
            $editorsPublication = $this->EditorsPublications->newEntity();
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                if (array_key_exists('author_id', $data)) {
                    // For adding author publication link
                    $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $data);
                    if ($this->AuthorsPublications->save($authorsPublication)) {
                        $this->Flash->success(__('The author has been linked.'));
                        return $this->redirect(['action' => 'add', $flag, $id]);
                    }
                    $this->Flash->error(__('The publication could not be linked. Please, try again.'));
                } else {
                    // For adding editor publication link
                    $editorsPublication = $this->EditorsPublications->patchEntity($editorsPublication, $data);
                    if ($this->EditorsPublications->save($editorsPublication)) {
                        $this->Flash->success(__('The editor has been linked.'));
                        return $this->redirect(['action' => 'add', $flag, $id]);
                    }
                    $this->Flash->error(__('The publication could not be linked. Please, try again.'));
                }
            }
            $authorsPublications = $this->paginate($this->AuthorsPublications->find('all', ['order' => 'publication_id', 'contain' => ['Authors', 'Publications']])->where(['author_id' => $id]));
            $editorsPublications = $this->paginate($this->EditorsPublications->find('all', ['order' => 'publication_id', 'contain' => ['Authors', 'Publications']])->where(['editor_id' => $id]));
            $this->set(compact('id', 'authorsPublications', 'authorsPublication', 'editorsPublications', 'editorsPublication', 'flag'));
        } elseif ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'AuthorsPublications']);
            $this->BulkUpload->upload();
            
            $this->set(compact('flag'));
        }
    }

    /**
     * Edit method.
     *
     * @param int $id Author/Editor Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Normal edit,
     * 'author' => Edit page for a selected publication,
     * 'publication' => Edit page for a selected author/editor.
     * @param int $parent_id Id of the selected publication or selected author/editor for $flag = 'author' and $flag = 'publication' respectively.
     *
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = '', $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $authorsPublication = $this->AuthorsPublications->get($id, [
                        'contain' => ['Publications', 'Authors']
                    ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $this->request->getData());
            if ($this->AuthorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                return $this->redirect(['action' => 'add', $flag, $parent_id]);
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $authorsPublications = $this->AuthorsPublications->find('all', ['contain' => ['Authors', 'Publications']])->where(['publication_id' => $authorsPublication->publication_id])->all();
        $this->set(compact('authorsPublication', 'flag', 'parent_id', 'authorsPublications'));
    }

    /**
     * Delete method.
     *
     * @param int $id Author/Editor Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Index page,
     * 'author' => Add page for a selected publication,
     * 'publication' => Add page for a selected author/editor.
     *
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = '', $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $authorsPublication = $this->AuthorsPublications->get($id);
        if ($this->AuthorsPublications->delete($authorsPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add', $flag, $parent_id]);
    }

    /**
     * Export method for downloading the entries containing errors.
     */
    public function export()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->loadComponent('BulkUpload', ['table' => 'AuthorsPublications']);
        $this->BulkUpload->export();
    }
}
