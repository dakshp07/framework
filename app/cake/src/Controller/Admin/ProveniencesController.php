<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Proveniences Controller
 *
 * @property \App\Model\Table\ProveniencesTable $Proveniences
 *
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProveniencesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'contain' => ['Regions']
        ];
        $proveniences = $this->paginate($this->Proveniences);

        $this->set(compact('proveniences'));
    }

    /**
     * View method
     *
     * @param string|null $id Provenience id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $provenience = $this->Proveniences->get($id, [
            'contain' => ['Regions', 'Archives', 'Artifacts', 'Dynasties']
        ]);

        $this->set('provenience', $provenience);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $provenience = $this->Proveniences->newEntity();
        if ($this->request->is('post')) {
            $provenience = $this->Proveniences->patchEntity($provenience, $this->request->getData());
            if ($this->Proveniences->save($provenience)) {
                $this->Flash->success(__('The provenience has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The provenience could not be saved. Please, try again.'));
        }
        $regions = $this->Proveniences->Regions->find('list', ['limit' => 200]);
        $this->set(compact('provenience', 'regions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Provenience id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $provenience = $this->Proveniences->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $provenience = $this->Proveniences->patchEntity($provenience, $this->request->getData());
            if ($this->Proveniences->save($provenience)) {
                $this->Flash->success(__('The provenience has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The provenience could not be saved. Please, try again.'));
        }
        $regions = $this->Proveniences->Regions->find('list', ['limit' => 200]);
        $this->set(compact('provenience', 'regions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Provenience id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $provenience = $this->Proveniences->get($id);
        if ($this->Proveniences->delete($provenience)) {
            $this->Flash->success(__('The provenience has been deleted.'));
        } else {
            $this->Flash->error(__('The provenience could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
