<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Publications Controller
 *
 * @property \App\Model\Table\PublicationsTable $Publications
 *
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Bibliography');
        $this->loadComponent('TableExport');
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'EntryTypes',
                'Journals',
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                ],
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                ]
            ],
            'order' => [
                'COALESCE(Authors.author, "zz") ASC',
                'COALESCE(Publications.title, "zz") ASC',
                'COALESCE(Publications.designation, "zz") ASC'
            ]
        ];

        $query = $this->Publications
            ->find('all')
            ->distinct(['Publications.id'])
            // ->where(['Publications.accepted' => 1])
            ->leftJoinWith('Authors')
            ->where(['OR' => ['sequence' => 0, 'Authors.id IS NULL']]);

        $queryParams = $this->request->getQueryParams();
        if (!empty($queryParams)) {
            $searchFilter = array();
            $schema = $this->Publications->getSchema();

            // Apply filters
            foreach ($queryParams as $field => $value) {
                $value = trim($value);

                if (empty($value)) {
                    continue;
                }

                if ($field == 'entry_type_id' or $field == 'journal_id') {
                    $searchFilter["Publications.{$field}"] = $value;
                } elseif ($field == 'from') {
                    $searchFilter["Publications.year >="] = $value;
                } elseif ($field == 'to') {
                    $searchFilter["Publications.year <="] = $value;
                } elseif ($field == 'author') {
                    $filter = ['Authors.author LIKE' => "%{$value}%"];
                    $query = $query->innerJoinWith(
                        'Authors',
                        function ($q) use ($filter) {
                            return $q->where($filter);
                        }
                    );
                } elseif ($field == 'artifact') {
                    $filter = ['Artifacts.id' => ltrim($value, 'P0'), 'Artifacts.is_public' => 1];
                    $query = $query->innerJoinWith(
                        'Artifacts',
                        function ($q) use ($filter) {
                            return $q->where($filter);
                        }
                    );
                } elseif ($schema->hasColumn($field)) {
                    $searchFilter["Publications.{$field} LIKE"] = "%{$value}%";
                }
            }

            $this->paginate['conditions'] = $searchFilter;
        }

        $publications = $this->paginate($query);
        $entryTypes = $this->Publications->EntryTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'label',
            'order' => 'label'
        ])->toArray();
        $journals = $this->Publications->Journals->find('list', [
            'keyField' => 'id',
            'valueField' => 'journal',
            'order' => 'journal'
        ])->toArray();

        $this->set(compact('publications', 'entryTypes', 'journals'));
        $this->set('data', $queryParams);
        $this->set('_serialize', 'publications');
    }

    /**
     * View method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Allow only users with proper permissions to view private artifacts
        $condition = (!$this->GeneralFunctions->checkIfRolesExists([1,4])) ?  ['Artifacts.is_public' => true] : [];
        $publication = $this->Publications->get($id, [
            'contain' => [
                'Artifacts' => [
                    'Proveniences',
                    'Periods',
                    'ArtifactTypes',
                    'Collections',
                    'sort' => 'Artifacts.id',
                    'conditions' => $condition
                ],
                'EntryTypes',
                'Journals',
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                    ],
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                    ]
                ]
        ]);

        $is_admin = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('publication', 'is_admin'));
        $this->set('_serialize', 'publication');
    }
}
