<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsGenre Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property int|null $genre_id
 * @property bool|null $is_genre_uncertain
 * @property string|null $comments
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Genre $genre
 */
class ArtifactsGenre extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'genre_id' => true,
        'is_genre_uncertain' => true,
        'comments' => true,
        'artifact' => true,
        'genre' => true
    ];
}
