<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PostingTypes Model
 *
 * @property \App\Model\Table\PostingsTable|\Cake\ORM\Association\HasMany $Postings
 *
 * @method \App\Model\Entity\PostingType get($primaryKey, $options = [])
 * @method \App\Model\Entity\PostingType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PostingType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PostingType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PostingType|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PostingType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PostingType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PostingType findOrCreate($search, callable $callback = null, $options = [])
 */
class PostingTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posting_types');
        $this->setDisplayField('posting_type');
        $this->setPrimaryKey('id');

        $this->hasMany('Postings', [
            'foreignKey' => 'posting_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('posting_type')
            ->maxLength('posting_type', 50)
            ->allowEmpty('posting_type')
            ->add('posting_type', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->isUnique(['posting_type']));

        return $rules;
    }
}
