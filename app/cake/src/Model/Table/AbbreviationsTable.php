<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Abbreviations Model
 *
 * @property \App\Model\Table\PublicationsTable|\Cake\ORM\Association\BelongsTo $Publications
 *
 * @method \App\Model\Entity\Abbreviation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Abbreviation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Abbreviation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Abbreviation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Abbreviation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Abbreviation findOrCreate($search, callable $callback = null, $options = [])
 */
class AbbreviationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('abbreviations');
        $this->setDisplayField('abbreviation');
        $this->setPrimaryKey('id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('abbreviation')
            ->maxLength('abbreviation', 100)
            ->requirePresence('abbreviation', 'create')
            ->notEmpty('abbreviation');

        $validator
            ->scalar('fullform')
            ->allowEmpty('fullform');

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['publication_id'], 'Publications'));

        return $rules;
    }
}
