<?php
namespace App\View;

use App\Model\Entity\Inscription;
use Cake\View\SerializedView;

class AtfView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'atf';

    protected function _serialize($serialize)
    {
        $inscriptions = $this->_dataToSerialize($serialize);

        return implode("\n", array_filter(array_map(function ($inscription) {
            return $inscription->atf;
        }, $inscriptions)));
    }
}
