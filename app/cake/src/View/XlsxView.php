<?php
namespace App\View;

use CsvView\View\CsvView;

class XlsxView extends CsvView
{
    use TableTrait;

    protected $_responseType = 'xlsx';

    protected function _serialize()
    {
        $data = $this->_rowsToSerialize($this->viewVars['_serialize']);
        $this->set([
            'table' => $this->prepareTableData($data),
            '_header' => $this->prepareTableHeader($data),
            '_serialize' => 'table'
        ]);
        return parent::_serialize();
    }
}
