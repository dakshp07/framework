<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="signReadings view content">
            <h3><?=h($signReading->sign_name)?></h3>
            <table class="table-bootstrap">
              <tr>
                <th>Sign Name</th>
                <td><?=h($signReading->sign_name) ?></td>
              </tr>
              <tr>
                <th>Sign Reading</th>
                <td><?=h($signReading->sign_reading) ?></td>
              </tr>
              <tr>
                <th>Meaning</th>
                <td><?=h($signReading->meaning)?></td>
              </tr>
            </table>
        </div>
    </div>
</div>

<div class="boxed mx-0">
    <?php if (empty($signReading->sign_readings_comments)): ?>
        <div class="capital-heading"><?= __('No Related Comments') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Comments') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Comment') ?></th>
            </thead>
            <tbody>
                <?php foreach ($signReading->sign_readings_comments as $comments): ?>
                <tr>
                    <td><?= h($comments->comment) ?></td>
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
