<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 login-box">
        <div class="capital-heading text-center">
            <h1 class="display-4 header-txt">Forgot Password</h1>
        </div>
            <?= $this->Flash->render() ?>

            <?= $this->Form->create() ?>
            <?= $this->Form->control('email', [
                'class' => 'col-12 input-background-child-md',
                'required' => true
            ]) ?>
            <div class="userloginbuttoncenter">
                <?= $this->Form->submit('submit', [
                    'class' => 'btn cdli-btn-blue'
                ]) ?>
            </div>
            <?= $this->Form->end() ?>
    </div>
</div>

<script language="JavaScript" type="text/javascript">
    $(document).ready(function () {
                setTimeout(function(){
                  location.reload(true);
                }, 15*60*1000);       
            });    
</script>