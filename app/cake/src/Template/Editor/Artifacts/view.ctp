<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Artifact'), ['action' => 'edit', $artifact->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Artifact'), ['action' => 'delete', $artifact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifact->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifact'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Periods'), ['controller' => 'Periods', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period'), ['controller' => 'Periods', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifact Types'), ['controller' => 'ArtifactTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifact Type'), ['controller' => 'ArtifactTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Archives'), ['controller' => 'Archives', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Archive'), ['controller' => 'Archives', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts Composites'), ['controller' => 'ArtifactsComposites', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifacts Composite'), ['controller' => 'ArtifactsComposites', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts Date Referenced'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifacts Date Referenced'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts Seals'), ['controller' => 'ArtifactsSeals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifacts Seal'), ['controller' => 'ArtifactsSeals', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts Shadow'), ['controller' => 'ArtifactsShadow', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifacts Shadow'), ['controller' => 'ArtifactsShadow', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Inscriptions'), ['controller' => 'Inscriptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Inscription'), ['controller' => 'Inscriptions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Retired Artifacts'), ['controller' => 'RetiredArtifacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Retired Artifact'), ['controller' => 'RetiredArtifacts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List External Resources'), ['controller' => 'ExternalResources', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New External Resource'), ['controller' => 'ExternalResources', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="artifacts view large-9 medium-8 columns content">
    <h3><?= h($artifact->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ark No') ?></th>
            <td><?= h($artifact->ark_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primary Publication Comments') ?></th>
            <td><?= h($artifact->primary_publication_comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cdli Collation') ?></th>
            <td><?= h($artifact->cdli_collation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Composite No') ?></th>
            <td><?= h($artifact->composite_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Condition Description') ?></th>
            <td><?= h($artifact->condition_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($artifact->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Comments') ?></th>
            <td><?= h($artifact->date_comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($artifact->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Designation') ?></th>
            <td><?= h($artifact->designation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Electronic Publication') ?></th>
            <td><?= h($artifact->electronic_publication) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Elevation') ?></th>
            <td><?= h($artifact->elevation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Excavation No') ?></th>
            <td><?= h($artifact->excavation_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Findspot Square') ?></th>
            <td><?= h($artifact->findspot_square) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Join Information') ?></th>
            <td><?= h($artifact->join_information) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lineart Up') ?></th>
            <td><?= h($artifact->lineart_up) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Museum No') ?></th>
            <td><?= h($artifact->museum_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artifact Preservation') ?></th>
            <td><?= h($artifact->artifact_preservation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Photo Up') ?></th>
            <td><?= h($artifact->photo_up) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Seal No') ?></th>
            <td><?= h($artifact->seal_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stratigraphic Level') ?></th>
            <td><?= h($artifact->stratigraphic_level) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Surface Preservation') ?></th>
            <td><?= h($artifact->surface_preservation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Provenience') ?></th>
            <td><?= $artifact->has('provenience') ? $this->Html->link($artifact->provenience->id, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Period') ?></th>
            <td><?= $artifact->has('period') ? $this->Html->link($artifact->period->id, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artifact Type') ?></th>
            <td><?= $artifact->has('artifact_type') ? $this->Html->link($artifact->artifact_type->id, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Archive') ?></th>
            <td><?= $artifact->has('archive') ? $this->Html->link($artifact->archive->id, ['controller' => 'Archives', 'action' => 'view', $artifact->archive->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= h($artifact->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($artifact->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Credit Id') ?></th>
            <td><?= $this->Number->format($artifact->credit_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Height') ?></th>
            <td><?= $this->Number->format($artifact->height) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Thickness') ?></th>
            <td><?= $this->Number->format($artifact->thickness) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Width') ?></th>
            <td><?= $this->Number->format($artifact->width) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Accounting Period') ?></th>
            <td><?= $this->Number->format($artifact->accounting_period) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Written In') ?></th>
            <td><?= $this->Number->format($artifact->written_in) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Object Type Uncertain') ?></th>
            <td><?= $this->Number->format($artifact->is_object_type_uncertain) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Weight') ?></th>
            <td><?= $this->Number->format($artifact->weight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dates Referenced') ?></th>
            <td><?= h($artifact->dates_referenced) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Public') ?></th>
            <td><?= $artifact->is_public ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Atf Public') ?></th>
            <td><?= $artifact->is_atf_public ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Are Images Public') ?></th>
            <td><?= $artifact->are_images_public ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Provenience Uncertain') ?></th>
            <td><?= $artifact->is_provenience_uncertain ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Period Uncertain') ?></th>
            <td><?= $artifact->is_period_uncertain ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is School Text') ?></th>
            <td><?= $artifact->is_school_text ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Cdli Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->cdli_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Findspot Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->findspot_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Seal Information') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->seal_information)); ?>
    </div>
    <div class="row">
        <h4><?= __('General Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->general_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Accession No') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->accession_no)); ?>
    </div>
    <div class="row">
        <h4><?= __('Alternative Years') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->alternative_years)); ?>
    </div>
    <div class="row">
        <h4><?= __('Dumb2') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->dumb2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Custom Designation') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->custom_designation)); ?>
    </div>
    <div class="row">
        <h4><?= __('Period Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->period_comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Provenience Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($artifact->provenience_comments)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Credits') ?></h4>
        <?php if (!empty($artifact->credits)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Comments') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->credits as $credits): ?>
            <tr>
                <td><?= h($credits->id) ?></td>
                <td><?= h($credits->user_id) ?></td>
                <td><?= h($credits->artifact_id) ?></td>
                <td><?= h($credits->date) ?></td>
                <td><?= h($credits->comments) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Credits', 'action' => 'view', $credits->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Credits', 'action' => 'edit', $credits->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Credits', 'action' => 'delete', $credits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $credits->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Collections') ?></h4>
        <?php if (!empty($artifact->collections)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Collection') ?></th>
                <th scope="col"><?= __('Geo Coordinates') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Is Private') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->collections as $collections): ?>
            <tr>
                <td><?= h($collections->id) ?></td>
                <td><?= h($collections->collection) ?></td>
                <td><?= h($collections->geo_coordinates) ?></td>
                <td><?= h($collections->slug) ?></td>
                <td><?= h($collections->is_private) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Collections', 'action' => 'view', $collections->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Collections', 'action' => 'edit', $collections->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Collections', 'action' => 'delete', $collections->id], ['confirm' => __('Are you sure you want to delete # {0}?', $collections->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related External Resources') ?></h4>
        <?php if (!empty($artifact->external_resources)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('External Resource') ?></th>
                <th scope="col"><?= __('Base Url') ?></th>
                <th scope="col"><?= __('Project Url') ?></th>
                <th scope="col"><?= __('Abbrev') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->external_resources as $externalResources): ?>
            <tr>
                <td><?= h($externalResources->id) ?></td>
                <td><?= h($externalResources->external_resource) ?></td>
                <td><?= h($externalResources->base_url) ?></td>
                <td><?= h($externalResources->project_url) ?></td>
                <td><?= h($externalResources->abbrev) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ExternalResources', 'action' => 'view', $externalResources->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ExternalResources', 'action' => 'edit', $externalResources->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ExternalResources', 'action' => 'delete', $externalResources->id], ['confirm' => __('Are you sure you want to delete # {0}?', $externalResources->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Genres') ?></h4>
        <?php if (!empty($artifact->genres)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Genre') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Genre Comments') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->genres as $genres): ?>
            <tr>
                <td><?= h($genres->id) ?></td>
                <td><?= h($genres->genre) ?></td>
                <td><?= h($genres->parent_id) ?></td>
                <td><?= h($genres->genre_comments) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Genres', 'action' => 'view', $genres->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Genres', 'action' => 'edit', $genres->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Genres', 'action' => 'delete', $genres->id], ['confirm' => __('Are you sure you want to delete # {0}?', $genres->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Languages') ?></h4>
        <?php if (!empty($artifact->languages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Order') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Language') ?></th>
                <th scope="col"><?= __('Protocol Code') ?></th>
                <th scope="col"><?= __('Inline Code') ?></th>
                <th scope="col"><?= __('Notes') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->languages as $languages): ?>
            <tr>
                <td><?= h($languages->id) ?></td>
                <td><?= h($languages->order) ?></td>
                <td><?= h($languages->parent_id) ?></td>
                <td><?= h($languages->language) ?></td>
                <td><?= h($languages->protocol_code) ?></td>
                <td><?= h($languages->inline_code) ?></td>
                <td><?= h($languages->notes) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Languages', 'action' => 'view', $languages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Languages', 'action' => 'edit', $languages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Languages', 'action' => 'delete', $languages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $languages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Materials') ?></h4>
        <?php if (!empty($artifact->materials)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Material') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->materials as $materials): ?>
            <tr>
                <td><?= h($materials->id) ?></td>
                <td><?= h($materials->material) ?></td>
                <td><?= h($materials->parent_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Materials', 'action' => 'view', $materials->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Materials', 'action' => 'edit', $materials->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Materials', 'action' => 'delete', $materials->id], ['confirm' => __('Are you sure you want to delete # {0}?', $materials->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Publications') ?></h4>
        <?php if (!empty($artifact->publications)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Bibtexkey') ?></th>
                <th scope="col"><?= __('Year') ?></th>
                <th scope="col"><?= __('Entry Type Id') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Annote') ?></th>
                <th scope="col"><?= __('Book Title') ?></th>
                <th scope="col"><?= __('Chapter') ?></th>
                <th scope="col"><?= __('Crossref') ?></th>
                <th scope="col"><?= __('Edition') ?></th>
                <th scope="col"><?= __('Editor') ?></th>
                <th scope="col"><?= __('How Published') ?></th>
                <th scope="col"><?= __('Institution') ?></th>
                <th scope="col"><?= __('Journal Id') ?></th>
                <th scope="col"><?= __('Month') ?></th>
                <th scope="col"><?= __('Note') ?></th>
                <th scope="col"><?= __('Number') ?></th>
                <th scope="col"><?= __('Organization') ?></th>
                <th scope="col"><?= __('Pages') ?></th>
                <th scope="col"><?= __('Publisher') ?></th>
                <th scope="col"><?= __('School') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Volume') ?></th>
                <th scope="col"><?= __('Publication History') ?></th>
                <th scope="col"><?= __('Abbreviation Id') ?></th>
                <th scope="col"><?= __('Series') ?></th>
                <th scope="col"><?= __('Oclc') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->publications as $publications): ?>
            <tr>
                <td><?= h($publications->id) ?></td>
                <td><?= h($publications->bibtexkey) ?></td>
                <td><?= h($publications->year) ?></td>
                <td><?= h($publications->entry_type_id) ?></td>
                <td><?= h($publications->address) ?></td>
                <td><?= h($publications->annote) ?></td>
                <td><?= h($publications->book_title) ?></td>
                <td><?= h($publications->chapter) ?></td>
                <td><?= h($publications->crossref) ?></td>
                <td><?= h($publications->edition) ?></td>
                <td><?= h($publications->editor) ?></td>
                <td><?= h($publications->how_published) ?></td>
                <td><?= h($publications->institution) ?></td>
                <td><?= h($publications->journal_id) ?></td>
                <td><?= h($publications->month) ?></td>
                <td><?= h($publications->note) ?></td>
                <td><?= h($publications->number) ?></td>
                <td><?= h($publications->organization) ?></td>
                <td><?= h($publications->pages) ?></td>
                <td><?= h($publications->publisher) ?></td>
                <td><?= h($publications->school) ?></td>
                <td><?= h($publications->title) ?></td>
                <td><?= h($publications->volume) ?></td>
                <td><?= h($publications->publication_history) ?></td>
                <td><?= h($publications->abbreviation_id) ?></td>
                <td><?= h($publications->series) ?></td>
                <td><?= h($publications->oclc) ?></td>
                <td><?= h($publications->designation) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Publications', 'action' => 'view', $publications->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Publications', 'action' => 'edit', $publications->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Publications', 'action' => 'delete', $publications->id], ['confirm' => __('Are you sure you want to delete # {0}?', $publications->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Composites') ?></h4>
        <?php if (!empty($artifact->artifacts_composites)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Composite') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->artifacts_composites as $artifactsComposites): ?>
            <tr>
                <td><?= h($artifactsComposites->id) ?></td>
                <td><?= h($artifactsComposites->composite_no) ?></td>
                <td><?= h($artifactsComposites->artifact_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsComposites', 'action' => 'view', $artifactsComposites->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsComposites', 'action' => 'edit', $artifactsComposites->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsComposites', 'action' => 'delete', $artifactsComposites->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsComposites->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Date Referenced') ?></h4>
        <?php if (!empty($artifact->artifacts_date_referenced)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Ruler Id') ?></th>
                <th scope="col"><?= __('Month Id') ?></th>
                <th scope="col"><?= __('Month No') ?></th>
                <th scope="col"><?= __('Year Id') ?></th>
                <th scope="col"><?= __('Day No') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->artifacts_date_referenced as $artifactsDateReferenced): ?>
            <tr>
                <td><?= h($artifactsDateReferenced->id) ?></td>
                <td><?= h($artifactsDateReferenced->artifact_id) ?></td>
                <td><?= h($artifactsDateReferenced->ruler_id) ?></td>
                <td><?= h($artifactsDateReferenced->month_id) ?></td>
                <td><?= h($artifactsDateReferenced->month_no) ?></td>
                <td><?= h($artifactsDateReferenced->year_id) ?></td>
                <td><?= h($artifactsDateReferenced->day_no) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'view', $artifactsDateReferenced->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'edit', $artifactsDateReferenced->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsDateReferenced', 'action' => 'delete', $artifactsDateReferenced->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDateReferenced->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Seals') ?></h4>
        <?php if (!empty($artifact->artifacts_seals)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Seal No') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->artifacts_seals as $artifactsSeals): ?>
            <tr>
                <td><?= h($artifactsSeals->id) ?></td>
                <td><?= h($artifactsSeals->seal_no) ?></td>
                <td><?= h($artifactsSeals->artifact_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsSeals', 'action' => 'view', $artifactsSeals->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsSeals', 'action' => 'edit', $artifactsSeals->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsSeals', 'action' => 'delete', $artifactsSeals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsSeals->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artifacts Shadow') ?></h4>
        <?php if (!empty($artifact->artifacts_shadow)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Cdli Comments') ?></th>
                <th scope="col"><?= __('Collection Location') ?></th>
                <th scope="col"><?= __('Collection Comments') ?></th>
                <th scope="col"><?= __('Acquisition History') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->artifacts_shadow as $artifactsShadow): ?>
            <tr>
                <td><?= h($artifactsShadow->id) ?></td>
                <td><?= h($artifactsShadow->artifact_id) ?></td>
                <td><?= h($artifactsShadow->cdli_comments) ?></td>
                <td><?= h($artifactsShadow->collection_location) ?></td>
                <td><?= h($artifactsShadow->collection_comments) ?></td>
                <td><?= h($artifactsShadow->acquisition_history) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ArtifactsShadow', 'action' => 'view', $artifactsShadow->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ArtifactsShadow', 'action' => 'edit', $artifactsShadow->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ArtifactsShadow', 'action' => 'delete', $artifactsShadow->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsShadow->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Inscriptions') ?></h4>
        <?php if (!empty($artifact->inscriptions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Transliteration') ?></th>
                <th scope="col"><?= __('Transliteration Clean') ?></th>
                <th scope="col"><?= __('Tranliteration Sign Names') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Credit Id') ?></th>
                <th scope="col"><?= __('Is Latest') ?></th>
                <th scope="col"><?= __('Annotation') ?></th>
                <th scope="col"><?= __('Atf2conll Diff Resolved') ?></th>
                <th scope="col"><?= __('Atf2conll Diff Unresolved') ?></th>
                <th scope="col"><?= __('Comments') ?></th>
                <th scope="col"><?= __('Structure') ?></th>
                <th scope="col"><?= __('Translations') ?></th>
                <th scope="col"><?= __('Transcriptions') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->inscriptions as $inscriptions): ?>
            <tr>
                <td><?= h($inscriptions->id) ?></td>
                <td><?= h($inscriptions->artifact_id) ?></td>
                <td><?= h($inscriptions->transliteration) ?></td>
                <td><?= h($inscriptions->transliteration_clean) ?></td>
                <td><?= h($inscriptions->tranliteration_sign_names) ?></td>
                <td><?= h($inscriptions->user_id) ?></td>
                <td><?= h($inscriptions->created) ?></td>
                <td><?= h($inscriptions->credit_id) ?></td>
                <td><?= h($inscriptions->is_latest) ?></td>
                <td><?= h($inscriptions->annotation) ?></td>
                <td><?= h($inscriptions->atf2conll_diff_resolved) ?></td>
                <td><?= h($inscriptions->atf2conll_diff_unresolved) ?></td>
                <td><?= h($inscriptions->comments) ?></td>
                <td><?= h($inscriptions->structure) ?></td>
                <td><?= h($inscriptions->translations) ?></td>
                <td><?= h($inscriptions->transcriptions) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Inscriptions', 'action' => 'view', $inscriptions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Inscriptions', 'action' => 'edit', $inscriptions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Inscriptions', 'action' => 'delete', $inscriptions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscriptions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Retired Artifacts') ?></h4>
        <?php if (!empty($artifact->retired_artifacts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('New Artifact Id') ?></th>
                <th scope="col"><?= __('Artifact Remarks') ?></th>
                <th scope="col"><?= __('Is Public') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artifact->retired_artifacts as $retiredArtifacts): ?>
            <tr>
                <td><?= h($retiredArtifacts->id) ?></td>
                <td><?= h($retiredArtifacts->artifact_id) ?></td>
                <td><?= h($retiredArtifacts->new_artifact_id) ?></td>
                <td><?= h($retiredArtifacts->artifact_remarks) ?></td>
                <td><?= h($retiredArtifacts->is_public) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'RetiredArtifacts', 'action' => 'view', $retiredArtifacts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'RetiredArtifacts', 'action' => 'edit', $retiredArtifacts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'RetiredArtifacts', 'action' => 'delete', $retiredArtifacts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $retiredArtifacts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
