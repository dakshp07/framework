<?php 
/*
 * Display date in view without formatting
 * 
*/
Cake\I18n\Date::setToStringFormat('YYYY-MM-dd');
Cake\I18n\FrozenDate::setToStringFormat('YYYY-MM-dd');
    
\Cake\Database\Type::build('date')
    ->useImmutable()
    ->useLocaleParser()
    ->setLocaleFormat('YYYY-MM-dd');
?>

<div class="container">

<h1 class="display-3 text-left header-txt">CDLI tablet</h1>
    <p class="text-left home-desc mt-4">View existing entries</p>
    <div class="card">
        <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table">
            <tbody>
                <?php foreach ($cdli_tablet as $results): ?>
                    <tr align="left">
                    <div class="card" style="width: 66.7rem;">
                        <div style="background-color:black;padding:50px;0px;">
                            <?php echo "<img src='https://cdli.ucla.edu//dl//daily_tablets////$results->imagefilename' style='width:50%;height:auto;'>" ?>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title"><?php echo "<b><a href='".$this->Url->build(["controller"=>"CdliTablet", "action"=>"view", $results->displaydate])."'>$results->theme: $results->shorttitle ($results->displaydate)</a></b>" ?></h4>
                            <p></p>
                            <p class="card-text"><i><?= strip_tags($results->shortdesc, '<a><abbr><b><br><i><center><div><em><p><q>') ?></i></p>
                            <p></p><hr><p></p>
                            <p class="card-text"><?= strip_tags($results->longdesc, '<a><abbr><b><br><i><center><div><em><p><q>') ?></p>
                            <p class="card-text"><i>Created by: <?= h($results->createdby) ?></i></p>
                        </div>
                    </div>
                </div>
                <br>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        </div>
    </div>
<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
</div>
<?= $this->Scroll->toTop()?>
