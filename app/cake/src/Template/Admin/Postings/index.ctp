<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting[]|\Cake\Collection\CollectionInterface $postings
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Posting'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Posting Types'), ['controller' => 'PostingTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting Type'), ['controller' => 'PostingTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Postings') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('posting_type_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col"><?= $this->Paginator->sort('published') ?></th>
            <th scope="col"><?= $this->Paginator->sort('lang') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publish_start') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publish_end') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($postings as $posting): ?>
        <tr>
            <!-- <td><?= $this->Number->format($posting->id) ?></td> -->
            <td><?= $posting->has('posting_type') ? $this->Html->link($posting->posting_type->posting_type, ['controller' => 'PostingTypes', 'action' => 'view', $posting->posting_type->id]) : '' ?></td>
            <td><?= h($posting->slug) ?></td>
            <td><?= h($posting->created) ?></td>
            <td><?= h($posting->modified) ?></td>
            <td><?= h($posting->published) ?></td>
            <td><?= h($posting->lang) ?></td>
            <td><?= $this->Number->format($posting->created_by) ?></td>
            <td><?= h($posting->modified_by) ?></td>
            <td><?= h($posting->publish_start) ?></td>
            <td><?= h($posting->publish_end) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $posting->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $posting->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $posting->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $posting->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

