<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsLanguage $artifactsLanguage
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsLanguage) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Language') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts]);
                echo $this->Form->control('language_id', ['options' => $languages]);
                echo $this->Form->control('is_language_uncertain');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsLanguage->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsLanguage->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts Languages'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
