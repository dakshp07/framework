<?php 
/*
 * Display date in view without formatting
 * 
*/
Cake\I18n\Date::setToStringFormat('YYYY-MM-dd');
Cake\I18n\FrozenDate::setToStringFormat('YYYY-MM-dd');
    
\Cake\Database\Type::build('date')
    ->useImmutable()
    ->useLocaleParser()
    ->setLocaleFormat('YYYY-MM-dd');
?>

<!-- jQuery Datepicker -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- End -->

<div class="container">
<div>
    <h1 class="display-3 text-left header-txt">CDLI tablet</h1>
    <p class="text-left home-desc mt-4">Manage existing entries.</p>
    <div class="card">
        <div class="card-body">
        <!-- Back button -->
        <table>
            <form class="form-inline">
                <tbody>
                    <td>
                        <?php echo $this->Html->link(
                            '<< Back', 
                            array('controller'=>'Home', 'action'=>'dashboard'), 
                            array('class'=>'btn btn-dark'));
                        ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link(
                            'View existing entries', 
                            '/CdliTablet',
                            //array('controller'=>'CdliTablet', 'action'=>'view'), 
                            array('class'=>'btn btn-primary'));
                        ?>
                    </td>
                </tbody>
            </form>
        </table>
        <br>
        <!-- End -->
        <div class="container" align="center">
            <div class="card">
                <div class="card-body">
                <br>
        <!-- Search bar -->
        <?= $this->Form->create(null, ['type'=>'get']) ?>
        <table>
            <form class="form-inline">
                <tbody>
                    <td>
                        <div class="form-group mx-sm-1 mb-2">
                            <?php echo $this->Form->input('key', ['label'=>'Search for entries by their theme:', 'value'=>$this->request->getQuery('key'), 'class'=>'form-group mx-sm-3 mb-2', 'placeholder'=>'Enter theme']) ?>
                        </div>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary mb-3">Search</button>
                    </td>
                </tbody>
            </form>
        </table>
        <?= $this->Form->end() ?>
        <!-- End -->

        <!-- Date to date search -->
        <?= $this->Form->create(null, ['type'=>'get']) ?>
        <table>
            <form class="form-inline">
                <tbody>
                    <td>
                        <div class="form-group mx-sm-1 mb-2">
                            <?php echo $this->Form->input('start_date', ['label'=>'Date to date search: ', 'class'=>'datepicker form-group mx-sm-3 mb-2', 'value'=>$this->request->getQuery('start_date'), 'placeholder'=>'Enter starting date', 'required'=>true]) ?>
                        </div>
                    </td>
                    <td>
                        <div class="form-group mx-sm-1 mb-2">
                            <?php echo $this->Form->input('end_date', ['label'=>'and', 'class'=>'datepicker form-group mx-sm-4 mb-2', 'value'=>$this->request->getQuery('end_date'), 'placeholder'=>'Enter ending date', 'required'=>true]) ?>
                        </div>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary mb-3">Search</button>
                    </td>
                </tbody>
            </form>
        </table>
        <?= $this->Form->end() ?>
        <!-- End -->
        </div>
    </div>
</div>
        <!-- Add a new entry/delete selected-->
        <?= $this->Form->create(NULL, ['url'=>['controller'=>'CdliTablet', 'action'=>'deleteAll']]) ?>
        <table>
            <form class="form-inline">
                <tbody>
                    <td>
                    <br>
                        <?php echo $this->Html->link(
                            'Add a new entry', 
                            array('controller'=>'CdliTablet', 'action'=>'add'), 
                            array('class'=>'btn btn-primary'));
                        ?>
                    </td>
                    <td>
                    <br>
                        <button type="submit" class="btn btn-danger">Delete selected</button>
                    </td>
                </tbody>
            </form>
        </table>
        <br>
        <div class="table-content table-responsive">
        
        <table cellpadding="0" cellspacing="0" class="table table-bordered table-light">
            <thead align="center">
            <tr>
                <th>#</th>
                <th scope="col">Date</th>
                <th scope="col">Theme</th>
                <th scope="col">Title</th>
                <th scope="col">Short Description</th>
                <th scope="col">Edit</th>                            
                <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cdli_tablet as $results): ?>
                <tr align="center">
                    <td><?= $this->Form->checkbox('ids[]', ['value'=>$results->id]) ?></td>
                    <td><?= h($results->displaydate) ?></td>
                    <td><?= h($results->theme) ?></td>
                    <?php echo "<td><a href='".$this->Url->build(["controller"=>"CdliTablet", "action"=>"view", $results->displaydate])."'>$results->theme: $results->shorttitle</a></td>" ?>
                    <td><?= strip_tags($results->shortdesc, '<a><abbr><b><br><i><center><div><em><p><q>') ?></td>
                        <td> 
                            <?php echo $this->Html->link(
                                'Edit', 
                                array('controller'=>'CdliTablet', 'action'=>'edit', $results->id), 
                                array('class'=>'btn btn-warning'));
                            ?>
                        </td> 
                        <td> 
                            <?php echo $this->Html->link(
                                'Delete', 
                                array('controller'=>'CdliTablet', 'action'=>'delete', $results->id), 
                                array('class'=>'btn btn-danger'));
                            ?>
                        </td>  
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        </div>
        <?= $this->Form->end() ?>
    <!-- End -->
    </div>
    </div>
    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>
<?= $this->Scroll->toTop()?>

<!-- jQuery Datepicker -->
<script>
  $( function() {
    $( ".datepicker" ).datepicker({'dateFormat':'yy-mm-dd'});
  } );
</script>
<!-- End -->
