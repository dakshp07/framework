<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Date $date
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($date) ?>
            <legend class="capital-heading"><?= __('Edit Date') ?></legend>
            <?php
                echo $this->Form->control('day_no');
                echo $this->Form->control('day_remarks');
                echo $this->Form->control('month_id', ['options' => $months, 'empty' => true]);
                echo $this->Form->control('is_uncertain');
                echo $this->Form->control('month_no');
                echo $this->Form->control('year_id', ['options' => $years, 'empty' => true]);
                echo $this->Form->control('dynasty_id', ['options' => $dynasties, 'empty' => true]);
                echo $this->Form->control('ruler_id', ['options' => $rulers, 'empty' => true]);
                echo $this->Form->control('absolute_year');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $date->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $date->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Dates'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Years'), ['controller' => 'Years', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Year'), ['controller' => 'Years', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Dynasties'), ['controller' => 'Dynasties', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Dynasty'), ['controller' => 'Dynasties', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
