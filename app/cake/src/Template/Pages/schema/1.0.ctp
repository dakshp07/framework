<div class="text-left">
    <section class="border-bottom mt-3">
        <h2 id="abbreviation">abbreviation</h2>
        <p>Abbreviations</p>
        <dl>
            <dt id="abbreviation-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="abbreviation-abbreviation">abbreviation <span class="badge badge-primary">string</span></dt>
            <dl>Abbreviation</dl>

            <dt id="abbreviation-fullform">fullform <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Fullform</dl>

            <dt id="abbreviation-type">type <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Type</dl>

            <dt id="abbreviation-publication_id">publication_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Publication Id</dl>

            <dt id="abbreviation-publications">publications <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#publication">oneToMany: publication</a></dt>
            <dl>Publications</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="agade_mail">agade_mail</h2>
        <p>Agade Mails</p>
        <dl>
            <dt id="agade_mail-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="agade_mail-title">title <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Title</dl>

            <dt id="agade_mail-date">date <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Date</dl>

            <dt id="agade_mail-category">category <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Category</dl>

            <dt id="agade_mail-content">content <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Content</dl>

            <dt id="agade_mail-is_public">is_public <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Public</dl>

            <dt id="agade_mail-cdli_tags">cdli_tags <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#entity">manyToMany: entity</a></dt>
            <dl>Cdli Tags</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="agade_mails_cdli_tag">agade_mails_cdli_tag</h2>
        <p>Agade Mails Cdli Tags</p>
        <dl>
            <dt id="agade_mails_cdli_tag-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="agade_mails_cdli_tag-agade_mail_id">agade_mail_id <span class="badge badge-primary">number</span></dt>
            <dl>Agade Mail Id</dl>

            <dt id="agade_mails_cdli_tag-cdli_tag_id">cdli_tag_id <span class="badge badge-primary">number</span></dt>
            <dl>Cdli Tag Id</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="archive">archive</h2>
        <p>Archives</p>
        <dl>
            <dt id="archive-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="archive-archive">archive <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Archive</dl>

            <dt id="archive-provenience_id">provenience_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Provenience Id</dl>

            <dt id="archive-provenience">provenience <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#provenience">manyToOne: provenience</a></dt>
            <dl>Provenience</dl>

            <dt id="archive-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">oneToMany: artifact</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="article">article</h2>
        <p>Articles</p>
        <dl>
            <dt id="article-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="article-title">title <span class="badge badge-primary">string</span></dt>
            <dl>Title</dl>

            <dt id="article-content_html">content_html <span class="badge badge-primary">string</span></dt>
            <dl>Content Html</dl>

            <dt id="article-content_latex">content_latex <span class="badge badge-primary">string</span></dt>
            <dl>Content Latex</dl>

            <dt id="article-is_pdf_uloaded">is_pdf_uloaded <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Pdf Uloaded</dl>

            <dt id="article-article_type">article_type <span class="badge badge-primary">number</span></dt>
            <dl>Article Type</dl>

            <dt id="article-is_published">is_published <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Published</dl>

            <dt id="article-created_by">created_by <span class="badge badge-primary">number</span></dt>
            <dl>Created By</dl>

            <dt id="article-created">created <span class="badge badge-primary">number</span></dt>
            <dl>Created</dl>

            <dt id="article-modified">modified <span class="badge badge-primary">number</span></dt>
            <dl>Modified</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="articles_author">articles_author</h2>
        <p>Articles Authors</p>
        <dl>
            <dt id="articles_author-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="articles_author-article_id">article_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Article Id</dl>

            <dt id="articles_author-author_id">author_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Author Id</dl>

            <dt id="articles_author-sequence">sequence <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="articles_publication">articles_publication</h2>
        <p>Articles Publications</p>
        <dl>
            <dt id="articles_publication-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="articles_publication-article_id">article_id <span class="badge badge-primary">number</span></dt>
            <dl>Article Id</dl>

            <dt id="articles_publication-publication_id">publication_id <span class="badge badge-primary">number</span></dt>
            <dl>Publication Id</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifact">artifact</h2>
        <p>Artifacts</p>
        <dl>
            <dt id="artifact-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifact-ark_no">ark_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Ark No</dl>

            <dt id="artifact-cdli_comments">cdli_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Cdli Comments</dl>

            <dt id="artifact-composite_no">composite_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Composite No</dl>

            <dt id="artifact-condition_description">condition_description <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Condition Description</dl>

            <dt id="artifact-designation">designation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Designation</dl>

            <dt id="artifact-elevation">elevation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Elevation</dl>

            <dt id="artifact-excavation_no">excavation_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Excavation No</dl>

            <dt id="artifact-findspot_comments">findspot_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Findspot Comments</dl>

            <dt id="artifact-findspot_square">findspot_square <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Findspot Square</dl>

            <dt id="artifact-museum_no">museum_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Museum No</dl>

            <dt id="artifact-artifact_preservation">artifact_preservation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Preservation</dl>

            <dt id="artifact-is_public">is_public <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Public</dl>

            <dt id="artifact-is_atf_public">is_atf_public <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Atf Public</dl>

            <dt id="artifact-are_images_public">are_images_public <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Are Images Public</dl>

            <dt id="artifact-seal_no">seal_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Seal No</dl>

            <dt id="artifact-seal_information">seal_information <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Seal Information</dl>

            <dt id="artifact-stratigraphic_level">stratigraphic_level <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Stratigraphic Level</dl>

            <dt id="artifact-surface_preservation">surface_preservation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Surface Preservation</dl>

            <dt id="artifact-thickness">thickness <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Thickness</dl>

            <dt id="artifact-height">height <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Height</dl>

            <dt id="artifact-width">width <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Width</dl>

            <dt id="artifact-weight">weight <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Weight</dl>

            <dt id="artifact-provenience_id">provenience_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Provenience Id</dl>

            <dt id="artifact-period_id">period_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Period Id</dl>

            <dt id="artifact-is_provenience_uncertain">is_provenience_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Provenience Uncertain</dl>

            <dt id="artifact-is_period_uncertain">is_period_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Period Uncertain</dl>

            <dt id="artifact-artifact_type_id">artifact_type_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Type Id</dl>

            <dt id="artifact-accession_no">accession_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Accession No</dl>

            <dt id="artifact-alternative_years">alternative_years <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Alternative Years</dl>

            <dt id="artifact-period_comments">period_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Period Comments</dl>

            <dt id="artifact-provenience_comments">provenience_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Provenience Comments</dl>

            <dt id="artifact-is_school_text">is_school_text <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is School Text</dl>

            <dt id="artifact-written_in">written_in <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Written In</dl>

            <dt id="artifact-is_artifact_type_uncertain">is_artifact_type_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Artifact Type Uncertain</dl>

            <dt id="artifact-archive_id">archive_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Archive Id</dl>

            <dt id="artifact-dates_referenced">dates_referenced <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Dates Referenced</dl>

            <dt id="artifact-accounting_period">accounting_period <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Accounting Period</dl>

            <dt id="artifact-artifact_comments">artifact_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Comments</dl>

            <dt id="artifact-created_by">created_by <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Created By</dl>

            <dt id="artifact-retired">retired <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Retired</dl>

            <dt id="artifact-has_fragments">has_fragments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Has Fragments</dl>

            <dt id="artifact-credits">credits <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#credit">oneToMany: credit</a></dt>
            <dl>Credits</dl>

            <dt id="artifact-provenience">provenience <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#provenience">manyToOne: provenience</a></dt>
            <dl>Provenience</dl>

            <dt id="artifact-period">period <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#period">manyToOne: period</a></dt>
            <dl>Period</dl>

            <dt id="artifact-artifact_type">artifact_type <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact_type">manyToOne: artifact_type</a></dt>
            <dl>Artifact Type</dl>

            <dt id="artifact-archive">archive <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#archive">manyToOne: archive</a></dt>
            <dl>Archive</dl>

            <dt id="artifact-artifacts_shadow">artifacts_shadow <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_shadow">oneToMany: artifacts_shadow</a></dt>
            <dl>Artifacts Shadow</dl>

            <dt id="artifact-inscriptions">inscriptions <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#inscription">oneToMany: inscription</a></dt>
            <dl>Inscriptions</dl>

            <dt id="artifact-retired_artifacts">retired_artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#retired_artifact">oneToMany: retired_artifact</a></dt>
            <dl>Retired Artifacts</dl>

            <dt id="artifact-collections">collections <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_collection">manyToMany: artifacts_collection</a></dt>
            <dl>Collections</dl>

            <dt id="artifact-dates">dates <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_date">manyToMany: artifacts_date</a></dt>
            <dl>Dates</dl>

            <dt id="artifact-external_resources">external_resources <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_external_resource">manyToMany: artifacts_external_resource</a></dt>
            <dl>External Resources</dl>

            <dt id="artifact-genres">genres <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_genre">manyToMany: artifacts_genre</a></dt>
            <dl>Genres</dl>

            <dt id="artifact-languages">languages <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_language">manyToMany: artifacts_language</a></dt>
            <dl>Languages</dl>

            <dt id="artifact-materials">materials <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_material">manyToMany: artifacts_material</a></dt>
            <dl>Materials</dl>

            <dt id="artifact-materials">components <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_material">manyToMany: artifacts_material</a></dt>
            <dl>Materials</dl>

            <dt id="artifact-publications">publications <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_publication">manyToMany: artifacts_publication</a></dt>
            <dl>Publications</dl>

            <dt id="artifact-witnesses">witnesses <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_composite">manyToMany: artifacts_composite</a></dt>
            <dl>Composite witnesses</dl>

            <dt id="artifact-impressions">impressions <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_seal">manyToMany: artifacts_seal</a></dt>
            <dl>Seal impressions</dl>

            <dt id="artifact-composites">composites <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_composite">manyToMany: artifacts_composite</a></dt>
            <dl>Artifact composites</dl>

            <dt id="artifact-seals">seals <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_seal">manyToMany: artifacts_seal</a></dt>
            <dl>Artifact seals</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_collection">artifacts_collection</h2>
        <p>Artifacts Collections</p>
        <dl>
            <dt id="artifacts_collection-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_collection-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_collection-collection_id">collection_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Collection Id</dl>

            <dt id="artifacts_collection-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_collection-collection">collection <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#collection">manyToOne: collection</a></dt>
            <dl>Collection</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_composite">artifacts_composite</h2>
        <p>Artifacts Composites</p>
        <dl>
            <dt id="artifacts_composite-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_composite-composite_no">composite_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Composite</dl>

            <dt id="artifacts_composite-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_composite-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_date">artifacts_date</h2>
        <p>Artifacts Dates</p>
        <dl>
            <dt id="artifacts_date-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_date-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_date-date_id">date_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Date Id</dl>

            <dt id="artifacts_date-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_date-date">date <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#date">manyToOne: date</a></dt>
            <dl>Date</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_external_resource">artifacts_external_resource</h2>
        <p>Artifacts External Resources</p>
        <dl>
            <dt id="artifacts_external_resource-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_external_resource-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_external_resource-external_resource_id">external_resource_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>External Resource Id</dl>

            <dt id="artifacts_external_resource-external_resource_key">external_resource_key <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>External Resource Key</dl>

            <dt id="artifacts_external_resource-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_external_resource-external_resource">external_resource <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#external_resource">manyToOne: external_resource</a></dt>
            <dl>External Resource</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_genre">artifacts_genre</h2>
        <p>Artifacts Genres</p>
        <dl>
            <dt id="artifacts_genre-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_genre-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_genre-genre_id">genre_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Genre Id</dl>

            <dt id="artifacts_genre-is_genre_uncertain">is_genre_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Genre Uncertain</dl>

            <dt id="artifacts_genre-comments">comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Comments</dl>

            <dt id="artifacts_genre-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_genre-genre">genre <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#genre">manyToOne: genre</a></dt>
            <dl>Genre</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_language">artifacts_language</h2>
        <p>Artifacts Languages</p>
        <dl>
            <dt id="artifacts_language-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_language-artifact_id">artifact_id <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_language-language_id">language_id <span class="badge badge-primary">number</span></dt>
            <dl>Language Id</dl>

            <dt id="artifacts_language-is_language_uncertain">is_language_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Language Uncertain</dl>

            <dt id="artifacts_language-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_language-language">language <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#language">manyToOne: language</a></dt>
            <dl>Language</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_material">artifacts_material</h2>
        <p>Artifacts Materials</p>
        <dl>
            <dt id="artifacts_material-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_material-artifact_id">artifact_id <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_material-material_id">material_id <span class="badge badge-primary">number</span></dt>
            <dl>Material Id</dl>

            <dt id="artifacts_material-is_material_uncertain">is_material_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Material Uncertain</dl>

            <dt id="artifacts_material-material_color_id">material_color_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Material Color Id</dl>

            <dt id="artifacts_material-material_aspect_id">material_aspect_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Material Aspect Id</dl>

            <dt id="artifacts_material-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_material-material">material <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#material">manyToOne: material</a></dt>
            <dl>Material</dl>

            <dt id="artifacts_material-material_color">material_color <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#material_color">manyToOne: material_color</a></dt>
            <dl>Material Color</dl>

            <dt id="artifacts_material-material_aspect">material_aspect <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#material_aspect">manyToOne: material_aspect</a></dt>
            <dl>Material Aspect</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_publication">artifacts_publication</h2>
        <p>Artifacts Publications</p>
        <dl>
            <dt id="artifacts_publication-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_publication-artifact_id">artifact_id <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_publication-publication_id">publication_id <span class="badge badge-primary">number</span></dt>
            <dl>Publication Id</dl>

            <dt id="artifacts_publication-exact_reference">exact_reference <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Exact Reference</dl>

            <dt id="artifacts_publication-publication_type">publication_type <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Publication Type</dl>

            <dt id="artifacts_publication-publication_comments">publication_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Publication Comments</dl>

            <dt id="artifacts_publication-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="artifacts_publication-publication">publication <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#publication">manyToOne: publication</a></dt>
            <dl>Publication</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_seal">artifacts_seal</h2>
        <p>Artifacts Seals</p>
        <dl>
            <dt id="artifacts_seal-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_seal-seal_no">seal_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Seal No</dl>

            <dt id="artifacts_seal-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_seal-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_shadow">artifacts_shadow</h2>
        <p>Artifacts Shadow</p>
        <dl>
            <dt id="artifacts_shadow-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_shadow-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_shadow-cdli_comments">cdli_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Cdli Comments</dl>

            <dt id="artifacts_shadow-collection_location">collection_location <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Collection Location</dl>

            <dt id="artifacts_shadow-collection_comments">collection_comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Collection Comments</dl>

            <dt id="artifacts_shadow-acquisition_history">acquisition_history <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Acquisition History</dl>

            <dt id="artifacts_shadow-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifacts_update">artifacts_update</h2>
        <p>Artifacts Updates</p>
        <dl>
            <dt id="artifacts_update-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifacts_update-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="artifacts_update-comments">comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Comments</dl>

            <dt id="artifacts_update-designation">designation <span class="badge badge-primary">string</span></dt>
            <dl>Designation</dl>

            <dt id="artifacts_update-artifact_type">artifact_type <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Type</dl>

            <dt id="artifacts_update-period">period <span class="badge badge-primary">string</span></dt>
            <dl>Period</dl>

            <dt id="artifacts_update-provenience">provenience <span class="badge badge-primary">string</span></dt>
            <dl>Provenience</dl>

            <dt id="artifacts_update-written_in">written_in <span class="badge badge-primary">string</span></dt>
            <dl>Written In</dl>

            <dt id="artifacts_update-archive">archive <span class="badge badge-primary">string</span></dt>
            <dl>Archive</dl>

            <dt id="artifacts_update-composite_no">composite_no <span class="badge badge-primary">string</span></dt>
            <dl>Composite No</dl>

            <dt id="artifacts_update-seal_no">seal_no <span class="badge badge-primary">string</span></dt>
            <dl>Seal No</dl>

            <dt id="artifacts_update-composites">composites <span class="badge badge-primary">string</span></dt>
            <dl>Composites</dl>

            <dt id="artifacts_update-seals">seals <span class="badge badge-primary">string</span></dt>
            <dl>Seals</dl>

            <dt id="artifacts_update-museum_no">museum_no <span class="badge badge-primary">string</span></dt>
            <dl>Museum No</dl>

            <dt id="artifacts_update-accession_no">accession_no <span class="badge badge-primary">string</span></dt>
            <dl>Accession No</dl>

            <dt id="artifacts_update-condition_description">condition_description <span class="badge badge-primary">string</span></dt>
            <dl>Condition Description</dl>

            <dt id="artifacts_update-artifact_preservation">artifact_preservation <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Preservation</dl>

            <dt id="artifacts_update-period_comments">period_comments <span class="badge badge-primary">string</span></dt>
            <dl>Period Comments</dl>

            <dt id="artifacts_update-provenience_comments">provenience_comments <span class="badge badge-primary">string</span></dt>
            <dl>Provenience Comments</dl>

            <dt id="artifacts_update-is_provenience_uncertain">is_provenience_uncertain <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Provenience Uncertain</dl>

            <dt id="artifacts_update-is_period_uncertain">is_period_uncertain <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Period Uncertain</dl>

            <dt id="artifacts_update-is_artifact_type_uncertain">is_artifact_type_uncertain <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Artifact Type Uncertain</dl>

            <dt id="artifacts_update-is_school_text">is_school_text <span class="badge badge-primary">boolean</span></dt>
            <dl>Is School Text</dl>

            <dt id="artifacts_update-height">height <span class="badge badge-primary">number</span></dt>
            <dl>Height</dl>

            <dt id="artifacts_update-thickness">thickness <span class="badge badge-primary">number</span></dt>
            <dl>Thickness</dl>

            <dt id="artifacts_update-width">width <span class="badge badge-primary">number</span></dt>
            <dl>Width</dl>

            <dt id="artifacts_update-weight">weight <span class="badge badge-primary">number</span></dt>
            <dl>Weight</dl>

            <dt id="artifacts_update-elevation">elevation <span class="badge badge-primary">string</span></dt>
            <dl>Elevation</dl>

            <dt id="artifacts_update-excavation_no">excavation_no <span class="badge badge-primary">string</span></dt>
            <dl>Excavation No</dl>

            <dt id="artifacts_update-findspot_square">findspot_square <span class="badge badge-primary">string</span></dt>
            <dl>Findspot Square</dl>

            <dt id="artifacts_update-findspot_comments">findspot_comments <span class="badge badge-primary">string</span></dt>
            <dl>Findspot Comments</dl>

            <dt id="artifacts_update-stratigraphic_level">stratigraphic_level <span class="badge badge-primary">string</span></dt>
            <dl>Stratigraphic Level</dl>

            <dt id="artifacts_update-surface_preservation">surface_preservation <span class="badge badge-primary">string</span></dt>
            <dl>Surface Preservation</dl>

            <dt id="artifacts_update-artifact_comments">artifact_comments <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Comments</dl>

            <dt id="artifacts_update-seal_information">seal_information <span class="badge badge-primary">string</span></dt>
            <dl>Seal Information</dl>

            <dt id="artifacts_update-is_public">is_public <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Public</dl>

            <dt id="artifacts_update-is_atf_public">is_atf_public <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Atf Public</dl>

            <dt id="artifacts_update-are_images_public">are_images_public <span class="badge badge-primary">number</span></dt>
            <dl>Are Images Public</dl>

            <dt id="artifacts_update-artifacts_collections">artifacts_collections <span class="badge badge-primary">number</span></dt>
            <dl>Artifacts Collections</dl>

            <dt id="artifacts_update-artifacts_composites">artifacts_composites <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Composites</dl>

            <dt id="artifacts_update-artifacts_seals">artifacts_seals <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Seals</dl>

            <dt id="artifacts_update-artifacts_dates">artifacts_dates <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Dates</dl>

            <dt id="artifacts_update-alternative_years">alternative_years <span class="badge badge-primary">string</span></dt>
            <dl>Alternative Years</dl>

            <dt id="artifacts_update-artifacts_genres">artifacts_genres <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Genres</dl>

            <dt id="artifacts_update-artifacts_languages">artifacts_languages <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Languages</dl>

            <dt id="artifacts_update-artifacts_materials">artifacts_materials <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Materials</dl>

            <dt id="artifacts_update-artifacts_shadow_cdli_comments">artifacts_shadow_cdli_comments <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Shadow Cdli Comments</dl>

            <dt id="artifacts_update-artifacts_shadow_collection_location">artifacts_shadow_collection_location <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Shadow Collection Location</dl>

            <dt id="artifacts_update-artifacts_shadow_collection_comments">artifacts_shadow_collection_comments <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Shadow Collection Comments</dl>

            <dt id="artifacts_update-artifacts_shadow_acquisition_history">artifacts_shadow_acquisition_history <span class="badge badge-primary">string</span></dt>
            <dl>Artifacts Shadow Acquisition History</dl>

            <dt id="artifacts_update-pubications_key">pubications_key <span class="badge badge-primary">string</span></dt>
            <dl>Pubications Key</dl>

            <dt id="artifacts_update-publications_type">publications_type <span class="badge badge-primary">string</span></dt>
            <dl>Publications Type</dl>

            <dt id="artifacts_update-publications_exact_ref">publications_exact_ref <span class="badge badge-primary">string</span></dt>
            <dl>Publications Exact Ref</dl>

            <dt id="artifacts_update-publications_comment">publications_comment <span class="badge badge-primary">string</span></dt>
            <dl>Publications Comment</dl>

            <dt id="artifacts_update-update_events_id">update_events_id <span class="badge badge-primary">number</span></dt>
            <dl>Update Events Id</dl>

            <dt id="artifacts_update-approved_by">approved_by <span class="badge badge-primary">number</span></dt>
            <dl>Approved By</dl>

            <dt id="artifacts_update-approved">approved <span class="badge badge-primary">boolean</span></dt>
            <dl>Approved</dl>

            <dt id="artifacts_update-publication_error">publication_error <span class="badge badge-primary">string</span></dt>
            <dl>Publication Error</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="artifact_type">artifact_type</h2>
        <p>Artifact Types</p>
        <dl>
            <dt id="artifact_type-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="artifact_type-artifact_type">artifact_type <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Type</dl>

            <dt id="artifact_type-parent_id">parent_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Parent Id</dl>

            <dt id="artifact_type-parent_artifact_type">parent_artifact_type <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact_type">manyToOne: artifact_type</a></dt>
            <dl>Parent Artifact Type</dl>

            <dt id="artifact_type-child_artifact_types">child_artifact_types <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact_type">oneToMany: artifact_type</a></dt>
            <dl>Child Artifact Types</dl>

            <dt id="artifact_type-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">oneToMany: artifact</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="author">author</h2>
        <p>Authors</p>
        <dl>
            <dt id="author-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="author-author">author <span class="badge badge-primary">string</span></dt>
            <dl>Author</dl>

            <dt id="author-last">last <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Last</dl>

            <dt id="author-first">first <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>First</dl>

            <dt id="author-east_asian_order">east_asian_order <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>East Asian Order</dl>

            <dt id="author-email">email <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Email</dl>

            <dt id="author-institution">institution <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Institution</dl>

            <dt id="author-modified">modified <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Modified</dl>

            <dt id="author-orcid_id">orcid_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Orcid Id</dl>

            <dt id="author-cdl_notes">cdl_notes <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#cdl_note">oneToMany: cdl_note</a></dt>
            <dl>Cdl Notes</dl>

            <dt id="author-credits">credits <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#credit">oneToMany: credit</a></dt>
            <dl>Credits</dl>

            <dt id="author-users">users <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#user">oneToMany: user</a></dt>
            <dl>Users</dl>

            <dt id="author-publications">publications <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#authors_publication">manyToMany: authors_publication</a></dt>
            <dl>Publications</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="authors_publication">authors_publication</h2>
        <p>Authors Publications</p>
        <dl>
            <dt id="authors_publication-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="authors_publication-publication_id">publication_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Publication Id</dl>

            <dt id="authors_publication-author_id">author_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Author Id</dl>

            <dt id="authors_publication-sequence">sequence <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="authors_publication-publication">publication <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#publication">manyToOne: publication</a></dt>
            <dl>Publication</dl>

            <dt id="authors_publication-author">author <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#author">manyToOne: author</a></dt>
            <dl>Author</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="authors_update_event">authors_update_event</h2>
        <p>Authors Update Events</p>
        <dl>
            <dt id="authors_update_event-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="authors_update_event-update_events_id">update_events_id <span class="badge badge-primary">number</span></dt>
            <dl>Update Events Id</dl>

            <dt id="authors_update_event-author_id">author_id <span class="badge badge-primary">number</span></dt>
            <dl>'credits to'</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="cdli_tag">cdli_tag</h2>
        <p>Cdli Tags</p>
        <dl>
            <dt id="cdli_tag-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="cdli_tag-cdli_tag">cdli_tag <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Cdli Tag</dl>

            <dt id="cdli_tag-agade_mails">agade_mails <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#entity">manyToMany: entity</a></dt>
            <dl>Agade Mails</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="collection">collection</h2>
        <p>Collections</p>
        <dl>
            <dt id="collection-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="collection-collection">collection <span class="badge badge-primary">string</span></dt>
            <dl>Collection</dl>

            <dt id="collection-geo_coordinates">geo_coordinates <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Geo Coordinates</dl>

            <dt id="collection-slug">slug <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Slug</dl>

            <dt id="collection-is_private">is_private <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Private</dl>

            <dt id="collection-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_collection">manyToMany: artifacts_collection</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="date">date</h2>
        <p>Dates</p>
        <dl>
            <dt id="date-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="date-day_no">day_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Day No</dl>

            <dt id="date-day_remarks">day_remarks <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Day Remarks</dl>

            <dt id="date-month_id">month_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Month Id</dl>

            <dt id="date-is_uncertain">is_uncertain <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Uncertain</dl>

            <dt id="date-month_no">month_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Month No</dl>

            <dt id="date-year_id">year_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Year Id</dl>

            <dt id="date-dynasty_id">dynasty_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Dynasty Id</dl>

            <dt id="date-ruler_id">ruler_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Ruler Id</dl>

            <dt id="date-absolute_year">absolute_year <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Absolute Year</dl>

            <dt id="date-month">month <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#month">manyToOne: month</a></dt>
            <dl>Month</dl>

            <dt id="date-year">year <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#year">manyToOne: year</a></dt>
            <dl>Year</dl>

            <dt id="date-dynasty">dynasty <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#dynasty">manyToOne: dynasty</a></dt>
            <dl>Dynasty</dl>

            <dt id="date-ruler">ruler <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#ruler">manyToOne: ruler</a></dt>
            <dl>Ruler</dl>

            <dt id="date-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_date">manyToMany: artifacts_date</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="dynastie">dynastie</h2>
        <p>Dynasties</p>
        <dl>
            <dt id="dynastie-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="dynastie-polity">polity <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Polity</dl>

            <dt id="dynastie-dynasty">dynasty <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Dynasty</dl>

            <dt id="dynastie-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="dynastie-provenience_id">provenience_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Provenience Id</dl>

            <dt id="dynastie-provenience">provenience <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#provenience">manyToOne: provenience</a></dt>
            <dl>Provenience</dl>

            <dt id="dynastie-dates">dates <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#date">oneToMany: date</a></dt>
            <dl>Dates</dl>

            <dt id="dynastie-rulers">rulers <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#ruler">oneToMany: ruler</a></dt>
            <dl>Rulers</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="editors_publication">editors_publication</h2>
        <p>Editors Publications</p>
        <dl>
            <dt id="editors_publication-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="editors_publication-publication_id">publication_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Publication Id</dl>

            <dt id="editors_publication-editor_id">editor_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Editor Id</dl>

            <dt id="editors_publication-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="editors_publication-publication">publication <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#publication">manyToOne: publication</a></dt>
            <dl>Publication</dl>

            <dt id="editors_publication-author">author <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#author">manyToOne: author</a></dt>
            <dl>Author</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="entry_type">entry_type</h2>
        <p>Entry Types</p>
        <dl>
            <dt id="entry_type-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="entry_type-label">label <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Label</dl>

            <dt id="entry_type-author">author <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Author</dl>

            <dt id="entry_type-title">title <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Title</dl>

            <dt id="entry_type-journal">journal <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Journal</dl>

            <dt id="entry_type-year">year <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Year</dl>

            <dt id="entry_type-volume">volume <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Volume</dl>

            <dt id="entry_type-pages">pages <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Pages</dl>

            <dt id="entry_type-number">number <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Number</dl>

            <dt id="entry_type-month">month <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Month</dl>

            <dt id="entry_type-eid">eid <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Eid</dl>

            <dt id="entry_type-note">note <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Note</dl>

            <dt id="entry_type-crossref">crossref <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Crossref</dl>

            <dt id="entry_type-keyword">keyword <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Keyword</dl>

            <dt id="entry_type-doi">doi <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Doi</dl>

            <dt id="entry_type-url">url <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Url</dl>

            <dt id="entry_type-file">file <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>File</dl>

            <dt id="entry_type-citeseerurl">citeseerurl <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Citeseerurl</dl>

            <dt id="entry_type-pdf">pdf <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Pdf</dl>

            <dt id="entry_type-abstract">abstract <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Abstract</dl>

            <dt id="entry_type-comment">comment <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Comment</dl>

            <dt id="entry_type-owner">owner <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Owner</dl>

            <dt id="entry_type-timestamp">timestamp <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Timestamp</dl>

            <dt id="entry_type-review">review <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Review</dl>

            <dt id="entry_type-search">search <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Search</dl>

            <dt id="entry_type-publisher">publisher <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Publisher</dl>

            <dt id="entry_type-editor">editor <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Editor</dl>

            <dt id="entry_type-series">series <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Series</dl>

            <dt id="entry_type-address">address <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Address</dl>

            <dt id="entry_type-edition">edition <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Edition</dl>

            <dt id="entry_type-howpublished">howpublished <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Howpublished</dl>

            <dt id="entry_type-lastchecked">lastchecked <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Lastchecked</dl>

            <dt id="entry_type-booktitle">booktitle <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Booktitle</dl>

            <dt id="entry_type-organization">organization <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Organization</dl>

            <dt id="entry_type-language">language <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Language</dl>

            <dt id="entry_type-chapter">chapter <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Chapter</dl>

            <dt id="entry_type-type">type <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Type</dl>

            <dt id="entry_type-school">school <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>School</dl>

            <dt id="entry_type-nationality">nationality <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Nationality</dl>

            <dt id="entry_type-yearfiled">yearfiled <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Yearfiled</dl>

            <dt id="entry_type-assignee">assignee <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Assignee</dl>

            <dt id="entry_type-day">day <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Day</dl>

            <dt id="entry_type-dayfiled">dayfiled <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Dayfiled</dl>

            <dt id="entry_type-monthfiled">monthfiled <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Monthfiled</dl>

            <dt id="entry_type-institution">institution <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Institution</dl>

            <dt id="entry_type-revision">revision <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Revision</dl>

            <dt id="entry_type-publications">publications <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#publication">oneToMany: publication</a></dt>
            <dl>Publications</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="external_resource">external_resource</h2>
        <p>External Resources</p>
        <dl>
            <dt id="external_resource-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="external_resource-external_resource">external_resource <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>External Resource</dl>

            <dt id="external_resource-base_url">base_url <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Base Url</dl>

            <dt id="external_resource-project_url">project_url <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Project Url</dl>

            <dt id="external_resource-abbrev">abbrev <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Abbrev</dl>

            <dt id="external_resource-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_external_resource">manyToMany: artifacts_external_resource</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="genre">genre</h2>
        <p>Genres</p>
        <dl>
            <dt id="genre-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="genre-genre">genre <span class="badge badge-primary">string</span></dt>
            <dl>Genre</dl>

            <dt id="genre-parent_id">parent_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Parent Id</dl>

            <dt id="genre-genre_description">genre_description <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Genre Description</dl>

            <dt id="genre-parent_genre">parent_genre <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#genre">manyToOne: genre</a></dt>
            <dl>Parent Genre</dl>

            <dt id="genre-child_genres">child_genres <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#genre">oneToMany: genre</a></dt>
            <dl>Child Genres</dl>

            <dt id="genre-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_genre">manyToMany: artifacts_genre</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="inscription">inscription</h2>
        <p>Inscriptions</p>
        <dl>
            <dt id="inscription-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="inscription-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="inscription-atf">atf <span class="badge badge-primary">string</span></dt>
            <dl>Atf</dl>

            <dt id="inscription-jtf">jtf <span class="badge badge-primary">string</span></dt>
            <dl>Jtf</dl>

            <dt id="inscription-transliteration">transliteration <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Transliteration</dl>

            <dt id="inscription-transliteration_clean">transliteration_clean <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Transliteration Clean</dl>

            <dt id="inscription-tranliteration_sign_names">tranliteration_sign_names <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Tranliteration Sign Names</dl>

            <dt id="inscription-annotation">annotation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Annotation</dl>

            <dt id="inscription-is_atf2conll_diff_resolved">is_atf2conll_diff_resolved <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Atf2conll Diff Resolved</dl>

            <dt id="inscription-comments">comments <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Comments</dl>

            <dt id="inscription-structure">structure <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Structure</dl>

            <dt id="inscription-translation">translation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Translation</dl>

            <dt id="inscription-transcription">transcription <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Transcription</dl>

            <dt id="inscription-accepted_by">accepted_by <span class="badge badge-primary">number</span></dt>
            <dl>Accepted By</dl>

            <dt id="inscription-accepted">accepted <span class="badge badge-primary">boolean</span></dt>
            <dl>Accepted</dl>

            <dt id="inscription-update_events_id">update_events_id <span class="badge badge-primary">number</span></dt>
            <dl>Update Events Id</dl>

            <dt id="inscription-inscription_comments">inscription_comments <span class="badge badge-primary">string</span></dt>
            <dl>Inscription Comments</dl>

            <dt id="inscription-is_latest">is_latest <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Latest</dl>

            <dt id="inscription-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="journal">journal</h2>
        <p>Journals</p>
        <dl>
            <dt id="journal-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="journal-journal">journal <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Journal</dl>

            <dt id="journal-publications">publications <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#publication">oneToMany: publication</a></dt>
            <dl>Publications</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="language">language</h2>
        <p>Languages</p>
        <dl>
            <dt id="language-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="language-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="language-parent_id">parent_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Parent Id</dl>

            <dt id="language-language">language <span class="badge badge-primary">string</span></dt>
            <dl>Language</dl>

            <dt id="language-protocol_code">protocol_code <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Protocol Code</dl>

            <dt id="language-inline_code">inline_code <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Inline Code</dl>

            <dt id="language-notes">notes <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Notes</dl>

            <dt id="language-parent_language">parent_language <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#language">manyToOne: language</a></dt>
            <dl>Parent Language</dl>

            <dt id="language-child_languages">child_languages <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#language">oneToMany: language</a></dt>
            <dl>Child Languages</dl>

            <dt id="language-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_language">manyToMany: artifacts_language</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="material">material</h2>
        <p>Materials</p>
        <dl>
            <dt id="material-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="material-material">material <span class="badge badge-primary">string</span></dt>
            <dl>Material</dl>

            <dt id="material-parent_id">parent_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Parent Id</dl>

            <dt id="material-parent_material">parent_material <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#material">manyToOne: material</a></dt>
            <dl>Parent Material</dl>

            <dt id="material-child_materials">child_materials <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#material">oneToMany: material</a></dt>
            <dl>Child Materials</dl>

            <dt id="material-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_material">manyToMany: artifacts_material</a></dt>
            <dl>Artifacts</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="material_aspect">material_aspect</h2>
        <p>Material Aspects</p>
        <dl>
            <dt id="material_aspect-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="material_aspect-material_aspect">material_aspect <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Material Aspect</dl>

            <dt id="material_aspect-artifacts_materials">artifacts_materials <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_material">oneToMany: artifacts_material</a></dt>
            <dl>Artifacts Materials</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="material_color">material_color</h2>
        <p>Material Colors</p>
        <dl>
            <dt id="material_color-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="material_color-material_color">material_color <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Material Color</dl>

            <dt id="material_color-artifacts_materials">artifacts_materials <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_material">oneToMany: artifacts_material</a></dt>
            <dl>Artifacts Materials</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="month">month</h2>
        <p>Months</p>
        <dl>
            <dt id="month-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="month-composite_month_name">composite_month_name <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Composite Month Name</dl>

            <dt id="month-month_no">month_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Month No</dl>

            <dt id="month-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="month-dates">dates <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#date">oneToMany: date</a></dt>
            <dl>Dates</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="period">period</h2>
        <p>Periods</p>
        <dl>
            <dt id="period-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="period-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="period-period">period <span class="badge badge-primary">string</span></dt>
            <dl>Period</dl>

            <dt id="period-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">oneToMany: artifact</a></dt>
            <dl>Artifacts</dl>

            <dt id="period-rulers">rulers <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#ruler">oneToMany: ruler</a></dt>
            <dl>Rulers</dl>

            <dt id="period-years">years <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#year">oneToMany: year</a></dt>
            <dl>Years</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="posting">posting</h2>
        <p>Postings</p>
        <dl>
            <dt id="posting-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="posting-posting_type_id">posting_type_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Posting Type Id</dl>

            <dt id="posting-slug">slug <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Slug</dl>

            <dt id="posting-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Artifact Id</dl>

            <dt id="posting-created">created <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Created</dl>

            <dt id="posting-modified">modified <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Modified</dl>

            <dt id="posting-published">published <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Published</dl>

            <dt id="posting-title">title <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Title</dl>

            <dt id="posting-body">body <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Body</dl>

            <dt id="posting-lang">lang <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Lang</dl>

            <dt id="posting-created_by">created_by <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Created By</dl>

            <dt id="posting-modified_by">modified_by <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Modified By</dl>

            <dt id="posting-publish_start">publish_start <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Publish Start</dl>

            <dt id="posting-publish_end">publish_end <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Publish End</dl>

            <dt id="posting-posting_type">posting_type <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#posting_type">manyToOne: posting_type</a></dt>
            <dl>Posting Type</dl>

            <dt id="posting-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="posting_type">posting_type</h2>
        <p>Posting Types</p>
        <dl>
            <dt id="posting_type-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="posting_type-posting_type">posting_type <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Posting Type</dl>

            <dt id="posting_type-postings">postings <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#posting">oneToMany: posting</a></dt>
            <dl>Postings</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="provenience">provenience</h2>
        <p>Proveniences</p>
        <dl>
            <dt id="provenience-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="provenience-provenience">provenience <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Provenience</dl>

            <dt id="provenience-region_id">region_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Region Id</dl>

            <dt id="provenience-geo_coordinates">geo_coordinates <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Geo Coordinates</dl>

            <dt id="provenience-region">region <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#region">manyToOne: region</a></dt>
            <dl>Region</dl>

            <dt id="provenience-archives">archives <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#archive">oneToMany: archive</a></dt>
            <dl>Archives</dl>

            <dt id="provenience-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">oneToMany: artifact</a></dt>
            <dl>Artifacts</dl>

            <dt id="provenience-dynasties">dynasties <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#dynasty">oneToMany: dynasty</a></dt>
            <dl>Dynasties</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="publication">publication</h2>
        <p>Publications</p>
        <dl>
            <dt id="publication-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="publication-bibtexkey">bibtexkey <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Bibtexkey</dl>

            <dt id="publication-year">year <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Year</dl>

            <dt id="publication-entry_type_id">entry_type_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Entry Type Id</dl>

            <dt id="publication-address">address <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Address</dl>

            <dt id="publication-annote">annote <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Annote</dl>

            <dt id="publication-book_title">book_title <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Book Title</dl>

            <dt id="publication-chapter">chapter <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Chapter</dl>

            <dt id="publication-crossref">crossref <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Crossref</dl>

            <dt id="publication-edition">edition <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Edition</dl>

            <dt id="publication-editor">editor <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Editor</dl>

            <dt id="publication-how_published">how_published <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>How Published</dl>

            <dt id="publication-institution">institution <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Institution</dl>

            <dt id="publication-journal_id">journal_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Journal Id</dl>

            <dt id="publication-month">month <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Month</dl>

            <dt id="publication-note">note <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Note</dl>

            <dt id="publication-number">number <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Number</dl>

            <dt id="publication-organization">organization <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Organization</dl>

            <dt id="publication-pages">pages <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Pages</dl>

            <dt id="publication-publisher">publisher <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Publisher</dl>

            <dt id="publication-school">school <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>School</dl>

            <dt id="publication-title">title <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Title</dl>

            <dt id="publication-volume">volume <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Volume</dl>

            <dt id="publication-publication_history">publication_history <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Publication History</dl>

            <dt id="publication-series">series <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Series</dl>

            <dt id="publication-oclc">oclc <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Oclc</dl>

            <dt id="publication-designation">designation <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Designation</dl>

            <dt id="publication-accepted_by">accepted_by <span class="badge badge-primary">number</span></dt>
            <dl>Accepted By</dl>

            <dt id="publication-accepted">accepted <span class="badge badge-primary">boolean</span></dt>
            <dl>Accepted</dl>

            <dt id="publication-entry_type">entry_type <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#entry_type">manyToOne: entry_type</a></dt>
            <dl>Entry Type</dl>

            <dt id="publication-journal">journal <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#journal">manyToOne: journal</a></dt>
            <dl>Journal</dl>

            <dt id="publication-abbreviation">abbreviation <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#abbreviation">manyToOne: abbreviation</a></dt>
            <dl>Abbreviation</dl>

            <dt id="publication-artifacts">artifacts <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifacts_publication">manyToMany: artifacts_publication</a></dt>
            <dl>Artifacts</dl>

            <dt id="publication-authors">authors <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#authors_publication">manyToMany: authors_publication</a></dt>
            <dl>Authors</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="region">region</h2>
        <p>Regions</p>
        <dl>
            <dt id="region-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="region-region">region <span class="badge badge-primary">string</span></dt>
            <dl>Region</dl>

            <dt id="region-geo_coordinates">geo_coordinates <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Geo Coordinates</dl>

            <dt id="region-proveniences">proveniences <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#provenience">oneToMany: provenience</a></dt>
            <dl>Proveniences</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="retired_artifact">retired_artifact</h2>
        <p>Retired Artifacts</p>
        <dl>
            <dt id="retired_artifact-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="retired_artifact-artifact_id">artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Artifact Id</dl>

            <dt id="retired_artifact-new_artifact_id">new_artifact_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>New Artifact Id</dl>

            <dt id="retired_artifact-retired_by">retired_by <span class="badge badge-primary">number</span></dt>
            <dl>Retired By</dl>

            <dt id="retired_artifact-retired_for">retired_for <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Retired For</dl>

            <dt id="retired_artifact-is_public">is_public <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Is Public</dl>

            <dt id="retired_artifact-artifact">artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#artifact">manyToOne: artifact</a></dt>
            <dl>Artifact</dl>

            <dt id="retired_artifact-new_artifact">new_artifact <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#entity">manyToOne: entity</a></dt>
            <dl>New Artifact</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="role">role</h2>
        <p>Roles</p>
        <dl>
            <dt id="role-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="role-name">name <span class="badge badge-primary">string</span></dt>
            <dl>Name</dl>

            <dt id="role-users">users <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#roles_user">manyToMany: roles_user</a></dt>
            <dl>Users</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="roles_user">roles_user</h2>
        <p>Roles Users</p>
        <dl>
            <dt id="roles_user-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="roles_user-role_id">role_id <span class="badge badge-primary">number</span></dt>
            <dl>Role Id</dl>

            <dt id="roles_user-user_id">user_id <span class="badge badge-primary">number</span></dt>
            <dl>User Id</dl>

            <dt id="roles_user-role">role <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#role">manyToOne: role</a></dt>
            <dl>Role</dl>

            <dt id="roles_user-user">user <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#user">manyToOne: user</a></dt>
            <dl>User</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="ruler">ruler</h2>
        <p>Rulers</p>
        <dl>
            <dt id="ruler-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="ruler-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="ruler-ruler">ruler <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Ruler</dl>

            <dt id="ruler-period_id">period_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Period Id</dl>

            <dt id="ruler-dynasty_id">dynasty_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Dynasty Id</dl>

            <dt id="ruler-period">period <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#period">manyToOne: period</a></dt>
            <dl>Period</dl>

            <dt id="ruler-dynasty">dynasty <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#dynasty">manyToOne: dynasty</a></dt>
            <dl>Dynasty</dl>

            <dt id="ruler-dates">dates <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#date">oneToMany: date</a></dt>
            <dl>Dates</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="sign_reading">sign_reading</h2>
        <p>Sign Readings</p>
        <dl>
            <dt id="sign_reading-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="sign_reading-sign_name">sign_name <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Sign Name</dl>

            <dt id="sign_reading-sign_reading">sign_reading <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Sign Reading</dl>

            <dt id="sign_reading-preferred_reading">preferred_reading <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Preferred Reading</dl>

            <dt id="sign_reading-period_id">period_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Period Id</dl>

            <dt id="sign_reading-provenience_id">provenience_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Provenience Id</dl>

            <dt id="sign_reading-language_id">language_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Language Id</dl>

            <dt id="sign_reading-meaning">meaning <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Meaning</dl>

            <dt id="sign_reading-period">period <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#period">manyToOne: period</a></dt>
            <dl>Period</dl>

            <dt id="sign_reading-provenience">provenience <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#provenience">manyToOne: provenience</a></dt>
            <dl>Provenience</dl>

            <dt id="sign_reading-language">language <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#language">manyToOne: language</a></dt>
            <dl>Language</dl>

            <dt id="sign_reading-sign_readings_comments">sign_readings_comments <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#sign_readings_comment">oneToMany: sign_readings_comment</a></dt>
            <dl>Sign Readings Comments</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="sign_readings_comment">sign_readings_comment</h2>
        <p>Sign Readings Comments</p>
        <dl>
            <dt id="sign_readings_comment-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="sign_readings_comment-sign_reading_id">sign_reading_id <span class="badge badge-primary">number</span></dt>
            <dl>Sign Reading Id</dl>

            <dt id="sign_readings_comment-comment">comment <span class="badge badge-primary">string</span></dt>
            <dl>Comment</dl>

            <dt id="sign_readings_comment-date">date <span class="badge badge-primary">number</span></dt>
            <dl>Date</dl>

            <dt id="sign_readings_comment-author_id">author_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Author Id</dl>

            <dt id="sign_readings_comment-sign_reading">sign_reading <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#sign_reading">manyToOne: sign_reading</a></dt>
            <dl>Sign Reading</dl>

            <dt id="sign_readings_comment-author">author <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#author">manyToOne: author</a></dt>
            <dl>Author</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="staff">staff</h2>
        <p>Staff</p>
        <dl>
            <dt id="staff-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="staff-author_id">author_id <span class="badge badge-primary">number</span></dt>
            <dl>Author Id</dl>

            <dt id="staff-staff_type_id">staff_type_id <span class="badge badge-primary">number</span></dt>
            <dl>Staff Type Id</dl>

            <dt id="staff-cdli_title">cdli_title <span class="badge badge-primary">string</span></dt>
            <dl>Cdli Title</dl>

            <dt id="staff-contribution">contribution <span class="badge badge-primary">string</span></dt>
            <dl>Contribution</dl>

            <dt id="staff-sequence">sequence <span class="badge badge-primary">string</span></dt>
            <dl>Sequence</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="staff_type">staff_type</h2>
        <p>Staff Types</p>
        <dl>
            <dt id="staff_type-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="staff_type-staff_type">staff_type <span class="badge badge-primary">string</span></dt>
            <dl>Staff Type</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="update_event">update_event</h2>
        <p>Update Events</p>
        <dl>
            <dt id="update_event-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="update_event-created">created <span class="badge badge-primary">number</span></dt>
            <dl>Created</dl>

            <dt id="update_event-created_by">created_by <span class="badge badge-primary">number</span></dt>
            <dl>Created By</dl>

            <dt id="update_event-project">project <span class="badge badge-primary">string</span></dt>
            <dl>Project</dl>

            <dt id="update_event-inscription_type">inscription_type <span class="badge badge-primary">number</span></dt>
            <dl>Inscription Type</dl>

            <dt id="update_event-event_comments">event_comments <span class="badge badge-primary">string</span></dt>
            <dl>Event Comments</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="user">user</h2>
        <p>Users</p>
        <dl>
            <dt id="user-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="user-username">username <span class="badge badge-primary">string</span></dt>
            <dl>Username</dl>

            <dt id="user-email">email <span class="badge badge-primary">string</span></dt>
            <dl>Email</dl>

            <dt id="user-author_id">author_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Author Id</dl>

            <dt id="user-last_login_at">last_login_at <span class="badge badge-primary">number</span></dt>
            <dl>Last Login At</dl>

            <dt id="user-active">active <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>Active</dl>

            <dt id="user-password">password <span class="badge badge-primary">string</span></dt>
            <dl>Password</dl>

            <dt id="user-2fa_key">2fa_key <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>2fa Key</dl>

            <dt id="user-2fa_status">2fa_status <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">boolean</span></dt>
            <dl>2fa Status</dl>

            <dt id="user-created_at">created_at <span class="badge badge-primary">number</span></dt>
            <dl>Created At</dl>

            <dt id="user-modified_at">modified_at <span class="badge badge-primary">number</span></dt>
            <dl>Modified At</dl>

            <dt id="user-hd_images_collection_id">hd_images_collection_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Hd Images Collection Id</dl>

            <dt id="user-token_pass">token_pass <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Token Pass</dl>

            <dt id="user-generated_at">generated_at <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Generated At</dl>

            <dt id="user-author">author <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#author">manyToOne: author</a></dt>
            <dl>Author</dl>
        </dl>
    </section>
    <section class="border-bottom mt-3">
        <h2 id="year">year</h2>
        <p>Years</p>
        <dl>
            <dt id="year-id">id <span class="badge badge-primary">number</span></dt>
            <dl>Id</dl>

            <dt id="year-year_no">year_no <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Year No</dl>

            <dt id="year-sequence">sequence <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Sequence</dl>

            <dt id="year-composite_year_name">composite_year_name <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">string</span></dt>
            <dl>Composite Year Name</dl>

            <dt id="year-period_id">period_id <span class="badge badge-secondary">Optional</span> <span class="badge badge-primary">number</span></dt>
            <dl>Period Id</dl>

            <dt id="year-period">period <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#period">manyToOne: period</a></dt>
            <dl>Period</dl>

            <dt id="year-dates">dates <span class="badge badge-secondary">Optional</span> <a class="badge badge-success" href="#date">oneToMany: date</a></dt>
            <dl>Dates</dl>
        </dl>
    </section>
</div>
