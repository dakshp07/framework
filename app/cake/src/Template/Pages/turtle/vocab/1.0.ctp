@prefix crm: <http://www.cidoc-crm.org/cidoc-crm/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix cdli: <https://cdli.ucla.edu/docs/vocab/0.1#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <http://www.w3.org/2002/07/owl#> .

[ rdf:type owl:Ontology
 ] .

#################################################################
#    Classes
#################################################################

###  http://www.cidoc-crm.org/cidoc-crm/E42_Identifier
crm:E42_Identifier rdf:type owl:Class .


###  http://www.cidoc-crm.org/cidoc-crm/E54_Dimension
crm:E54_Dimension rdf:type owl:Class .


###  http://www.cidoc-crm.org/cidoc-crm/E55_Type
crm:E55_Type rdf:type owl:Class .


###  http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit
crm:E58_Measurement_Unit rdf:type owl:Class .


###  https://cdli.ucla.edu/docs/vocab/0.1#dimension_height
cdli:dimension_height rdf:type owl:Class ;
                      rdfs:subClassOf cdli:dimension_length ;
                      rdfs:comment "Artifact height. Usually the secondary reading direction."@en ;
                      rdfs:label "Height"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#dimension_length
cdli:dimension_length rdf:type owl:Class ;
                      rdfs:subClassOf crm:E54_Dimension .


###  https://cdli.ucla.edu/docs/vocab/0.1#dimension_thickness
cdli:dimension_thickness rdf:type owl:Class ;
                         rdfs:subClassOf cdli:dimension_length ;
                         rdfs:comment "Artifact thickness. The remaining spatial dimension."@en ;
                         rdfs:label "Thickness"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#dimension_weight
cdli:dimension_weight rdf:type owl:Class ;
                      rdfs:subClassOf crm:E54_Dimension ;
                      rdfs:comment "Artifact weight."@en ;
                      rdfs:label "Weight"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#dimension_width
cdli:dimension_width rdf:type owl:Class ;
                     rdfs:subClassOf cdli:dimension_length ;
                     rdfs:comment "Artifact width. Usually the primary reading direction."@en ;
                     rdfs:label "Width"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#g
cdli:g rdf:type owl:Class ;
       rdfs:subClassOf crm:E58_Measurement_Unit ;
       rdfs:label "Gram"^^xsd:string .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_accession
cdli:identifier_accession rdf:type owl:Class ;
                          rdfs:subClassOf crm:E42_Identifier ;
                          rdfs:label "Accession number"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_ark
cdli:identifier_ark rdf:type owl:Class ;
                    rdfs:subClassOf crm:E42_Identifier ;
                    rdfs:comment "Formatted like [http://NMA/]ark:/NAAN/Name[Qualifier] though in the database, only the NAAN (21198) and the Name are used."@en ;
                    rdfs:label "Archive Resource Key"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_cdli
cdli:identifier_cdli rdf:type owl:Class ;
                     rdfs:subClassOf crm:E42_Identifier ;
                     rdfs:comment "CDLI numbers, formatted as P###### (where # are numbers 0–9). Issued for artifacts by the CDLI."@en ;
                     rdfs:label "CDLI number"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_composite
cdli:identifier_composite rdf:type owl:Class ;
                          rdfs:subClassOf crm:E42_Identifier ;
                          rdfs:comment "Composite numbers, formatted as Q###### (where # are numbers 0–9). Actual formatting might vary. Issued for composites by ORACC's QCat."@en ;
                          rdfs:label "Composite number"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_designation
cdli:identifier_designation rdf:type owl:Class ;
                            rdfs:subClassOf crm:E42_Identifier ;
                            rdfs:label "Designation"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_excavation
cdli:identifier_excavation rdf:type owl:Class ;
                           rdfs:subClassOf crm:E42_Identifier ;
                           rdfs:label "Excavation number"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_findspot
cdli:identifier_findspot rdf:type owl:Class ;
                         rdfs:subClassOf crm:E42_Identifier ;
                         rdfs:label "Findspot square"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_iso-639
cdli:identifier_iso-639 rdf:type owl:Class ;
                        rdfs:subClassOf crm:E42_Identifier ;
                        rdfs:comment "ISO 639-1:2002 language codes."^^xsd:string ;
                        rdfs:label "Inline language code"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_museum
cdli:identifier_museum rdf:type owl:Class ;
                       rdfs:subClassOf crm:E42_Identifier ;
                       rdfs:label "Museum number"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#identifier_seal
cdli:identifier_seal rdf:type owl:Class ;
                     rdfs:subClassOf crm:E42_Identifier ;
                     rdfs:comment "Seal numbers, formatted as S###### (where # are numbers 0–9). Actual formatting might vary. Issued for seals."@en ;
                     rdfs:label "Seal number"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#mm
cdli:mm rdf:type owl:Class ;
        rdfs:subClassOf crm:E58_Measurement_Unit ;
        rdfs:label "Millimeter"^^xsd:string .


###  https://cdli.ucla.edu/docs/vocab/0.1#type_artifact_type
cdli:type_artifact_type rdf:type owl:Class ;
                        rdfs:subClassOf crm:E55_Type ;
                        rdfs:label "Artifact type"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#type_aspect
cdli:type_aspect rdf:type owl:Class ;
                 rdfs:subClassOf crm:E55_Type ;
                 rdfs:label "Material aspect"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#type_color
cdli:type_color rdf:type owl:Class ;
                rdfs:subClassOf crm:E55_Type ;
                rdfs:label "Material color"@en .


###  https://cdli.ucla.edu/docs/vocab/0.1#type_genre
cdli:type_genre rdf:type owl:Class ;
                rdfs:subClassOf crm:E55_Type ;
                rdfs:label "Genre"@en .


###  Generated by the OWL API (version 4.5.13) https://github.com/owlcs/owlapi
