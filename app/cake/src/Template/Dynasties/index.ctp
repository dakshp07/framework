<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty[]|\Cake\Collection\CollectionInterface $dynasties
 */
?>


<h3 class="display-4 pt-3"><?= __('Dynasties') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('polity') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dynasties as $dynasty): ?>
        <tr  align="left">
            <td><?= h($dynasty->polity) ?></td>
            <td>
                <a href="/dynasties/<?=h($dynasty->id)?>">
                    <?= h($dynasty->dynasty) ?>
                </a>
            </td>
            <td><?= $this->Number->format($dynasty->sequence) ?></td>
            <td><?= $dynasty->has('provenience') ? $this->Html->link($dynasty->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $dynasty->provenience->id]) : '' ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

