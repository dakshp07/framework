<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>


<main class="row justify-content-md-center">
    
    <div class="col-lg-6 boxed bg-light rounded">

        <div class="row">
            <h4 class="col-sm capital-heading text-center"><?= __('Change Password') ?></h4>
        </div>
        <?= $this->Form->create('{{$user}}', ['type' => 'post']) ?>
        <?= $this->Flash->render() ?>


        <fieldset disabled>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">Username</label>
                        <input type="text" style="width:100%" id="input-username" readonly class="form-control form-control-alternative bg-white rounded border border-white" value=<?= h($user['username']) ?>>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label" >Email</label>
                        <input type="text" style="width:100%" id="input-email" readonly class="form-control form-control-alternative bg-white rounded border border-white" value=<?= h($user['email']) ?>>
                    </div>
                </div>
            </div>
        </fieldset>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label" for="new-password" >New Password</label>
                    <?= $this->Form->control('password', ['class' => 'col-12 input-background-child-md rounded', 'label' => false, 'id' => 'new-password', 'placeholder' => 'Type your new password']) ?>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label" for="confirm-password" >Confirm New Password</label>
                    <?= $this->Form->control('password_confirm', ['class' => 'col-12 input-background-child-md rounded', 'label' => false, 'type' => 'password', 'id' => 'confirm-password', 'placeholder' => 'Confirm your password']) ?>                   
                    
                </div>
            </div>
        </div>

        <div class="float-right">
            <?= $this->Form->submit("Save new password", ['class' => 'btn cdli-btn-blue ml-6']) ?>
        </div>
        <?= $this->Form->end() ?>

    </div>


</main>