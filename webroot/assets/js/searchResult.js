let modal = document.getElementById("search-modal-dialog");
let modalContent = document.getElementById("search-modal-content");
let accordions = document.querySelectorAll(".accordion-textbox");

$(document).ready(function () {
    var rowCount = document.querySelectorAll('[id^="row"]').length;

    $("#add").click(function () {
        rowCount = document.querySelectorAll('[id^="row"]').length;

        if (rowCount < 3) {
            rowCount++;

            var additional_search = `
                <div id="row${rowCount}">
                    <div class="container rectangle-23 p-3">
                        <div class="align-items-baseline">
                            <select type="dropdown" name="operator${rowCount}" class="mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left">
                                <option value="AND">AND</option>
                                <option value="OR">OR</option>
                            </select>
                
                            <button type="button" name="remove" id="${rowCount}" class="btn cdli-btn-light ml-3 float-right btn_remove">
                                <b>&times;</b>
                            </button>
                                
                            <div class="search-page-grid w-100-sm">
                                <input type="text" name="publication${rowCount}" class="form-control" id="input${rowCount + 1}" placeholder="Search for publications, provenience, collection no." required="required">
                                <select aria-label="Additional category" class="form-group mb-0" id="${rowCount + 1}">
                                    <!-- <option value="keyword${rowCount}">Keywords</option> -->
                                    <option value="publication${rowCount}">Publications</option>
                                    <option value="collection${rowCount}">Collections</option>
                                    <option value="provenience${rowCount}">Proveniences</option>
                                    <option value="period${rowCount}">Periods</option>
                                    <option value="inscription${rowCount}">Inscriptions</option>
                                    <!-- <option value="ID${rowCount}">ID's</option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            $("#dynamic_field").append(additional_search);
        }
    });

    $(document).on("click", ".btn_remove", function () {
        var button_id = $(this).attr("id");
        rowCount--;
        $("#row" + button_id + "").remove();
    });

    $(document).on("change", ".form-group", function () {
        var select_id = $(this).attr("id");
        var select_name = $(this).children("option:selected").val();
        $("#input" + select_id).attr("name", select_name);
    });
});

// modal styles for sm devices
window.addEventListener("resize", () => {
    if (window.innerWidth < 600) {
        modal.style.width = "100vw";
        modal.style.margin = "0";
        modalContent.style.border = "none";
    } else {
        modal.style.width = "auto";
        modal.style.margin = "1.75rem auto";
        modalContent.style.border = "1px solid rgba(0,0,0,0.2)";
    }
});

// filter modal methods
const expandAll = () => {
    let btns = event.srcElement.parentElement;
    btns.children[0].classList.add("text-dark");
    btns.children[0].disabled = true;
    btns.children[1].disabled = false;
    btns.children[1].classList.remove("text-dark");

    accordions.forEach((checkbox) => {
        checkbox.checked = true;
    });
};

const collapseAll = () => {
    let btns = event.srcElement.parentElement;
    btns.children[1].classList.add("text-dark");
    btns.children[1].disabled = true;
    btns.children[0].disabled = false;
    btns.children[0].classList.remove("text-dark");

    accordions.forEach((checkbox) => {
        checkbox.checked = false;
    });
};
