// Set default margins, width and height
var margin = {top: 0, right: 0, bottom: 10, left: 40};
var width = 1200 - margin.left - margin.right;
var height = 2000 - margin.top - margin.bottom;

// Add SVG to dendrogram chart container
var svg = d3.select("#dendrogram-chart")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom);

// Add Group element to the SVG and translate it to center
var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


// Get all the non-leaf node names
var parents  = new Set();
data.forEach(function(d) {
    parents.add(d.parent);
})

// Get max of leaf values for X-axis domain
var max = 0;
data.forEach(function(d) {
    if (!parents.has(d.material))
        max = Math.max(max, d.count)
})

// Define X-scale
var xScale =  d3.scaleLinear()
    .domain([0, max])
    .range([0, 700]);   // Max value of range must be less than size of mapped bar chart

// Define X-axis
var xAxis = d3.axisTop()
    .scale(xScale);

// Add Color Scale
var colorScale = d3.scaleOrdinal(d3.schemeCategory10)
    .domain(data.map(function(d) { 
        return d.material; 
    }));


// Set up a way to handle the data
var tree = d3.cluster()                     // This D3 API method setup the Dendrogram datum position
    .size([height, width - 760])            // Total width - bar chart width = Dendrogram chart width
    .separation(function separate(a, b) {
        return a.parent == b.parent         // 2 levels tree grouping for category
            || a.parent.parent == b.parent
            || a.parent == b.parent.parent ? 0.4 : 0.8;
        });

// Make all count values in the data integers from string(default)
data.forEach(function(d) {
    d.count = +d.count;
})
    

// Format data in tree form
var root = d3.stratify()
    .id(function(d) { return d.material; })
    .parentId(function(d) { return d.parent; })
    (data);

tree(root);

// Draw a line connecting every datum to its parent
var link = g.selectAll(".link")
    .data(root.descendants().slice(1))
    .enter().append("path")
        .attr("class", "link")
        .attr("d", function(d) {
            return "M" + d.y + "," + d.x
                    + "C" + (d.parent.y + 100) + "," + d.x
                    + " " + (d.parent.y + 100) + "," + d.parent.x
                    + " " + d.parent.y + "," + d.parent.x;
        });

// Setup position for every datum
// Apply different CSS classes to parents and leafs
var node = g.selectAll(".node")
    .data(root.descendants())
    .enter().append("g")
        .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

// Draw a small circle for every datum
node.append("circle")
    .attr("r", 4);

// Setup G for every leaf datum
var leafNodeG = g.selectAll(".node--leaf")
    .append("g")
        .attr("class", "node--leaf-g")
        .attr("transform", "translate(" + 8 + "," + -13 + ")");

leafNodeG.append("rect")
    .attr("class", "shadow")
    .style("fill", function (d) {return colorScale(d.data.material);})
    .attr("width", 2)
    .attr("height", 30)
    .attr("rx", 2)
    .attr("ry", 2)
    .transition()
        .duration(800)
        .attr("width", function (d) {return xScale(d.data.count);});

leafNodeG.append("text")
    .attr("dy", 19.5)
    .attr("x", 8)
    .style("text-anchor", "start")
    .text(function (d) {
        return d.data.material;
    });

// Write down text for every parent datum
var internalNode = g.selectAll(".node--internal");
internalNode.append("text")
    .attr("y", -10)
    .style("text-anchor", "middle")
    .text(function (d) {
        return d.data.material;
    });

// Attach axis on top of the first leaf datum
// Get first leaf node
var firstEndNode = g.select(".node--leaf");
var minYTransform = firstEndNode.attr('transform').split(',')[1].split(')')[0];
$('.node--leaf').each(function() {
    var currentNode = d3.select(this);
    currentYTransform = currentNode.attr('transform').split(',')[1].split(')')[0];
    currentYTransform = +currentYTransform
    
    if (currentYTransform < minYTransform) {
        minYTransform = currentYTransform;
        firstEndNode = currentNode;
    }
});

firstEndNode.insert("g")
    .attr("class", "xAxis")
    .attr("transform", "translate(" + 7 + "," + -14 + ")")
    .call(xAxis);

// Tick mark for X-axis
firstEndNode.insert("g")
    .attr("class", "grid")
    .attr("transform", "translate(7," + (height - 15) + ")")
    .call(d3.axisBottom()
        .scale(xScale)
        .ticks(5)
        .tickSize(-height, 0, 0)
        .tickFormat("")
    );

// Emphasize the Y-axis baseline
svg.selectAll(".grid").select("line")
    .style("stroke-dasharray", "20,1")
    .style("stroke", "black");

// Moving ball (initially place it outside of visibility area)
var ballG = svg.insert("g")
    .attr("class", "ballG")
    .attr("transform", "translate(" + (width+200) + "," + height/2 + ")");
    
ballG.insert("circle")
    .attr("class", "shadow")
    .style("fill", "steelblue")
    .attr("r", 5);
    
ballG.insert("text")
    .style("text-anchor", "middle")
    .attr("dy", 5)
    .text("0.0");

// Animation functions for mouse on and off events
d3.selectAll(".node--leaf-g")
    .on("mouseover", handleMouseOver)
    .on("mouseout", handleMouseOut);

// Helper Mouse event handlers
function handleMouseOver(d) {
    var leafG = d3.select(this);

    leafG.select("rect")
        .attr("stroke", "#4D4D4D")
        .attr("stroke-width", "2");

    var ballGMovement = ballG.transition()
        .duration(400)
        .attr("transform", "translate(" + (d.y
                + xScale(d.data.count) + 75) + ","
                + (d.x + 1.5) + ")");

    ballGMovement.select("circle")
        .style("fill", colorScale(d.data.material))
        .attr("r", 18);

    ballGMovement.select("text")
        .delay(300)
        .text(d.data.count);
}
    
function handleMouseOut() {
    var leafG = d3.select(this);

    leafG.select("rect")
        .attr("stroke-width", "0");
}