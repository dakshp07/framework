$(function () {
    $("#bibtexkeyAutocomplete").on("input", function () {
        var options = {};
        options.url = '/admin/publications/autocomplete' + '?term=' + $("#bibtexkeyAutocomplete").val();
        options.type = "GET";
        options.data = { "criteria": $("#bibtexkeyAutocomplete").val() };
        options.dataType = "json";
        options.success = function (data) {
            $("#bibtexkeyList").empty();
            for(var i in data)
            {
                $("#bibtexkeyList").append("<option value='" + 
                data[i] + "'></option>");
            }
        };
        $.ajax(options);
    });

    $.ajaxSetup({
        headers: {
          'X-CSRF-Token': $('[name="_csrfToken"]').val()
        }
      })

    $('#authors_input').tagsInput({
        width: 'auto',
        defaultText: 'Add Author',
        delimiter: ';',
        trimValue: true,
        minChars: 2,
        onAddTag: addAuthorsNotAutoSuggested
    })

    $('#authors_input_tag').on('input', function (e) {
      var search = $('#authors_input_tag').val()
      $('#input-foot-authors-parent').html('')
      if (search.length > 2) {
        populate_AuthorsSearch(search)
      }
    })

    $('#editors_input').tagsInput({
        width: 'auto',
        defaultText: 'Add Editor',
        delimiter: ';',
        trimValue: true,
        minChars: 2,
        onAddTag: addEditorsNotAutoSuggested
    })

    $('#editors_input_tag').on('input', function (e) {
      var search = $('#editors_input_tag').val()
      $('#input-foot-editors-parent').html('')
      if (search.length > 2) {
        populate_EditorsSearch(search)
      }
    })
  }
);

function addAuthorsNotAutoSuggested (author) {
    $.get('/admin/authors/search/' + author, function (data, status) {
    data = JSON.parse(data)
    var search_author_list = data.map(function(value) {
        return value.author;
      });
    var input_author_list = $('#authors_input').val().split(";");
    if ((!search_author_list.includes(author)) && (input_author_list[input_author_list.length-1] == author)) {
        if ($('#input-foot-authors-parent').html() == '') {
            $('#input-foot-authors-parent').append('<br>Author \"' + author + '\" does not exist')
        }
        $('#authors_input').removeTag(author)
    }
    })
}

function add_author_helper (author, search) {
    author = unescape(author)
    $('#authors_input').removeTag(search)
    $('#input-foot-authors-parent').html('')
    var input_author_list = $('#authors_input').val().split(";");
    if (input_author_list.includes(author)) {
        $('#input-foot-authors-parent').append('Author has already been added.')
    } else {
        $('#authors_input').addTag(author)
    }
}

function populate_AuthorsSearch (name) {
    $.get('/admin/authors/search/' + name, function (data, status) {
    data = JSON.parse(data)
    limit = data.length <= 5 ? data.length : 5
    for (var i = 0; i < limit; i++) {
        var authors = data[i].author
        $('#input-foot-authors-parent').append(
        '<button onclick="add_author_helper(\'' +
        escape(authors) +
        "','" +
        name +
        '\')" class="btn btn-outline-primary m-1">' +
            authors +
            ' </button>')
        }
    })
}

function addEditorsNotAutoSuggested (author) {
    $.get('/admin/authors/search/' + author, function (data, status) {
    data = JSON.parse(data)
    var search_author_list = data.map(function(value) {
        return value.author;
      });
    var input_author_list = $('#editors_input').val().split(";");
    if ((!search_author_list.includes(author)) && (input_author_list[input_author_list.length-1] == author)) {
        if ($('#input-foot-editors-parent').html() == '') {
            $('#input-foot-editors-parent').append('<br>Editor \"' + author + '\" does not exist')
        }
        $('#editors_input').removeTag(author)
    }
    })
}

function add_editor_helper (author, search) {
    author = unescape(author)
    $('#editors_input').removeTag(search)
    $('#input-foot-editors-parent').html('')
    var input_author_list = $('#editors_input').val().split(";");
    if (input_author_list.includes(author)) {
        $('#input-foot-editors-parent').append('Editor has already been added.')
    } else {
        $('#editors_input').addTag(author)
    }
}

function populate_EditorsSearch (name) {
    $.get('/admin/authors/search/' + name, function (data, status) {
    data = JSON.parse(data)
    limit = data.length <= 5 ? data.length : 5
    for (var i = 0; i < limit; i++) {
        var authors = data[i].author
        $('#input-foot-editors-parent').append(
        '<button onclick="add_editor_helper(\'' +
        escape(authors) +
        "','" +
        name +
        '\')" class="btn btn-outline-primary m-1">' +
            authors +
            ' </button>')
        }
    })
}
