$(document).ready(function (e) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('[name="_csrfToken"]').val()
    }
  })
})

function populate_authors (authors) {
  var authors = authors.split(',')
  for (var i = 0; i < authors.length; i++) {
    $('#article_author_input').addTag(authors[i])
  }
}

function addAuthorsNotAutoSuggested (author) {
  $.get('/admin/authors/search/' + author, function (data, status) {
    data = JSON.parse(data)
    if (data.length < 1) {
      addNewAuthor(author)
    }
  })
}

function addNewAuthor (author) {
  $.post('/admin/authors/add/' + author, function (data, status) {})
}

function add_tag_helper (author, search) {
  $('#article_author_input').addTag(author)
  $('#input-foot-tags-parent').html('')
  $('#article_author_input').removeTag(search)
}

function populate_AuthorsSearch (name) {
  $.get('/admin/authors/search/' + name, function (data, status) {
    data = JSON.parse(data)
    limit = data.length <= 5 ? data.length : 5
    $('#input-foot-tags-parent').html('')
    for (var i = 0; i < limit; i++) {
      var authors = data[i].author
      authors = authors.split(',')
      for (var j = 0; j < authors.length; j++) {
        if (authors[j].toLowerCase().includes(name)) {
          $('#input-foot-tags-parent').append(
            '<span onclick="add_tag_helper(\'' +
              authors[j] +
              "','" +
              name +
              '\')" class="input-foot-tags">' +
              authors[j] +
              ' </span>'
          )
        }
      }
    }
  })
}

function remove_cdlp_pdf () {
  $('#uploadCDLPPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdlj_pdf () {
  $('#uploadCDLJPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdlb_pdf () {
  $('#uploadCDLBPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdlj_latex () {
  $('#uploadCDLJLatex').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdlb_latex () {
  $('#uploadCDLBLatex').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdlJ_pdf () {
  $('#uploadCDLJPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdln_pdf () {
  $('#uploadCDLNPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function submit_cdlp () {
  var cdlp_title = $('input[name="cdlp_title"]').val()
  var cdlp_status = $('input[name="cdlp_status"]').val()
  var cdlp_pdf_link = $('input[name="cdlp_pdf_link"]').val()
  var authors = $('input[name="authors"]').val()
  if ([cdlp_title, cdlp_status, cdlp_pdf_link, authors].includes('')) {
    $('#cdlp_error').show()
    $('#cdlp_error').animate({ opacity: '.2' })
    $('#cdlp_error').animate({ opacity: '1' })
  } else {
    $('#form-cdlp').submit()
  }
}

function success_article_upload_pdf (link, TYPE) {
  $('.ajax-file-upload-container').show()
  $('#upload' + TYPE + 'Pdf').hide()

  $('#edit_' + TYPE.toLowerCase() + '_filename').html(link)
  $('.ajax-file-upload-red').html('delete')
  $('.ajax-file-upload-red').show()
  $('.ajax-file-upload-abort').attr(
    'onclick',
    'remove_' + TYPE.toLowerCase() + '_pdf()'
  )
  $('#' + TYPE.toLowerCase() + '_pdf_link').val(link)
}

function success_article_upload_latex (data, TYPE) {
  $('.ajax-file-upload-container').show()
  $('#artilce_latex_dir').val(data)

  $('.ajax-file-upload-red').show()
  $('.ajax-file-upload-abort').attr(
    'onclick',
    'remove_' + TYPE.toLowerCase() + '_latex()'
  )

  $('#showConvertHTMLButton').show()
}

function delete_article_show () {
  $('#delete_article_warning').show()
  $('#delete_article_button').hide()
}

function delete_article_close () {
  $('#delete_article_warning').hide()
  $('#delete_article_button').show()
}

function delete_article_confirm (id) {
  $.post('/admin/articles/delete/' + id, function (data, status) {
    window.location.href = '/admin/articles/add'
  })
}

function submit_cdln () {
  var cdln_title = $('input[name="cdln_title"]').val()
  var cdln_status = $('input[name="cdln_status"]').val()
  var cdln_pdf_link = $('input[name="cdln_pdf_link"]').val()
  var authors = $('input[name="authors"]').val()
  var notes_data = CKEDITOR.instances.cdln_editor.getData()
  $('#cdln_content').val(notes_data)
  if ([cdln_title, cdln_status, authors].includes('')) {
    $('#cdln_error').show()
    $('#cdln_error').animate({ opacity: '.2' })
    $('#cdln_error').animate({ opacity: '1' })
  } else {
    $('#form-cdln').submit()
  }
}

function submit_cdlb () {
  var cdlb_title = $('input[name="cdlb_title"]').val()
  var cdlb_status = $('#cdlb_status').val()
  var cdlb_pdf_link = $('#cdlb_pdf_link').val()

  var authors = $('input[name="authors"]').val()

  var data = CKEDITOR.instances.article_editor.getData()

  var HTML_content_cdlb = $('#artilce_html_content').val()
  var latex_content_cdlb = $('#article_latex_content').val()
  var html_data_from_ck = CKEDITOR.instances.article_editor.getData()

  if (html_data_from_ck !== '') {
    HTML_content_cdlb = html_data_from_ck
    $('#artilce_html_content').val(html_data_from_ck)
  }
  if ($('#latex_file_editor').val() !== '') {
    var lexer_content = $('#latex_file_editor').val()
    latex_content_cdlb = lexer_content
    $('#article_latex_content').val(lexer_content)
  }
  if ([HTML_content_cdlb, latex_content_cdlb].includes('')) {
    alert('Please verity convert HTML.')
  }
  if (
    [
      cdlb_title,
      cdlb_status,
      cdlb_pdf_link,
      authors,
      HTML_content_cdlb,
      latex_content_cdlb
    ].includes('')
  ) {
    console.log(
      '| ' +
        cdlb_title +
        ' | ' +
        cdlb_status +
        '|' +
        cdlb_pdf_link +
        '|' +
        authors +
        '|' +
        HTML_content_cdlb +
        '|' +
        latex_content_cdlb
    )

    $('#cdlb_error').show()
    $('#cdlb_error').animate({ opacity: '.2' })
    $('#cdlb_error').animate({ opacity: '1' })
  } else {
    $('#article-form').submit()
  }
}

function submit_cdlj () {
  var cdlj_title = $('input[name="cdlj_title"]').val()
  var cdlj_status = $('input[name="cdlj_status"]').val()
  var cdlj_pdf_link = $('input[name="cdlj_pdf_link"]').val()

  var authors = $('input[name="authors"]').val()

  var data = CKEDITOR.instances.article_editor.getData()

  var HTML_content_cdlj = $('#artilce_html_content').val()
  var latex_content_cdlj = $('#article_latex_content').val()
  var html_data_from_ck = CKEDITOR.instances.article_editor.getData()
  if (html_data_from_ck !== '') {
    HTML_content_cdlj = html_data_from_ck
    $('#artilce_html_content').val(html_data_from_ck)
  }
  if ($('#latex_file_editor').val() !== '') {
    var lexer_content = $('#latex_file_editor').val()
    latex_content_cdlj = lexer_content
    $('#article_latex_content').val(lexer_content)
  }
  if ([HTML_content_cdlj, latex_content_cdlj].includes('')) {
    alert('Please verity convert HTML.')
  }
  if (
    [
      cdlj_title,
      cdlj_status,
      cdlj_pdf_link,
      authors,
      HTML_content_cdlj,
      latex_content_cdlj
    ].includes('')
  ) {
    $('#cdlj_error').show()
    $('#cdlj_error').animate({ opacity: '.2' })
    $('#cdlj_error').animate({ opacity: '1' })
  } else {
    $('#article-form').submit()
  }
}

$(function () {
  $('#article_author_input').tagsInput({
    width: 'auto',
    defaultText: 'Add Author',
    onAddTag: addAuthorsNotAutoSuggested
  })

  $('#article_author_input_tag').on('input', function (e) {
    var search = $('#article_author_input_tag').val()
    if (search.length > 2) {
      populate_AuthorsSearch(search)
    }
  })

  $('#BibManagerUpload').uploadFile({
    url: '/admin/articles/bib/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'bib',
    acceptFiles: 'bib',
    autoSubmit: true,
    maxFileSize: 5000 * 1024,
    onSuccess: function (files, data, xhr, pd) {
      bib_manager_mapper(data)
    }
  })

  $('#uploadCDLPPdf').uploadFile({
    url: '/admin/articles/add/CDLP/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'pdf',
    acceptFiles: 'pdf',
    autoSubmit: true,
    maxFileSize: 5000 * 1024,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload_pdf(data, 'CDLP')
    }
  })

  $('#ImageManagerUploadAll').uploadFile({
    url: '/admin/articles/image/manager/upload/' + article_type,
    multiple: true,
    maxFileCount: 20,
    allowedTypes: '*',
    acceptFiles: '*',
    autoSubmit: true,
    maxFileSize: 5000 * 1024,
    onSuccess: function (files, data, xhr, pd) {
      $('.upx_name_c').append(
        "<option value='" + data + "'>" + data + '</option>'
      )
    }
  })

  $('#uploadCDLJLatex').uploadFile({
    url: '/admin/articles/add/CDLJ/latex/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: '*',
    acceptFiles: '*',
    autoSubmit: true,
    maxFileSize: 5000 * 1024,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload_latex(data, 'CDLJ')
    }
  })

  $('#uploadCDLBLatex').uploadFile({
    url: '/admin/articles/add/CDLB/latex/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: '*',
    acceptFiles: '*',
    autoSubmit: true,
    maxFileSize: 5000 * 1024,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload_latex(data, 'CDLB')
    }
  })

  $('#uploadCDLJPdf').uploadFile({
    url: '/admin/articles/add/CDLJ/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'pdf',
    acceptFiles: 'pdf',
    autoSubmit: true,
    maxFileSize: 11000000,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload_pdf(data, 'CDLJ')
    }
  })

  $('#uploadCDLBPdf').uploadFile({
    url: '/admin/articles/add/CDLB/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'pdf',
    acceptFiles: 'pdf',
    autoSubmit: true,
    maxFileSize: 11000000,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload_pdf(data, 'CDLB')
    }
  })

  $('#uploadCDLNPdf').uploadFile({
    url: '/admin/articles/add/CDLN/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'pdf',
    acceptFiles: 'pdf',
    autoSubmit: true,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload_pdf(data, 'CDLN')
    }
  })
})

var i = 0
function preview_loader () {
  $('#myProgress').animate({ opacity: '1' })
  $('#myBar').animate({ width: '0%' })
  $('#myBar').animate({ width: '100%' })
  $('#myProgress').animate({ opacity: '0' })
}

var renderEditorMath = {
  pArticleAuthors: document.getElementById('pArticleContent'),
  update: function (tex) {
    this.pArticleAuthors.innerHTML = tex
    MathJax.Hub.Queue(['Typeset', MathJax.Hub, this.formula])
  }
}

function toggle_cdln_preview (toggle) {
  if (toggle == 'show') {
    var title = $('input[name="cdln_title"]').val()
    var authors = $('input[name="cdln_authors"]').val()
    var notes_data = CKEDITOR.instances.cdln_editor.getData()
    authors = authors.split(',')
    $('#pArticleAuthors').html('')
    for (var j = 0; j < authors.length; j++) {
      if (j == authors.length - 1) {
        $('#pArticleAuthors').append(authors[j])
      } else {
        $('#pArticleAuthors').append(authors[j] + ', ')
      }
    }
    $('#pArticleName').html(title)
    renderEditorMath.update(notes_data)
    $('#articlePreviewDiv').show()
    $('#form-cdln').hide()
    $('#relatedActions').hide()
    $('#articleEditDiv')
      .removeClass('col-lg-7')
      .addClass('col-lg-2')
    $('#closePreviewButton').show()
    preview_loader()
  } else {
    $('#articlePreviewDiv').hide()
    $('#form-cdln').show()
    $('#relatedActions').show()
    $('#articleEditDiv')
      .removeClass('col-lg-2')
      .addClass('col-lg-7')
    $('#closePreviewButton').hide()
  }
  $('#articlePreviewDiv').animate({ opacity: '.2' })
  $('#articlePreviewDiv').animate({ opacity: '1' })
  $('#articleEditDiv').animate({ opacity: '.2' })
  $('#articleEditDiv').animate({ opacity: '1' })
}

function toggle_latex_preview (toggle) {
  if (toggle == 'show') {
    hide_all_converter_preview_tabs()
    toggle_converter_tab('EditHTML')
    convert_latex_to_html_api()
    edit_latex_tabhelper()

    $('#articleConvertDiv').show()
    $('#article-form').hide()
    $('#relatedActions').hide()
    $('#articleEditDiv')
      .removeClass('col-lg-7')
      .addClass('col-lg-2')

    $('#closeHTMLPreviewButton').show()
    $('#reConvertButton').show()
    $('#image_manager').show()
    $('#bib_manager').show()
    preview_loader()
  } else {
    $('#articleConvertDiv').hide()
    $('#article-form').show()
    $('#relatedActions').show()
    $('#articleEditDiv')
      .removeClass('col-lg-2')
      .addClass('col-lg-7')
    $('#reConvertButton').hide()
    $('#image_manager').hide()
    $('#bib_manager').hide()
    $('#closeHTMLPreviewButton').hide()
  }
  $('#articleConvertDiv').animate({ opacity: '.2' })
  $('#articleConvertDiv').animate({ opacity: '1' })
  $('#articleEditDiv').animate({ opacity: '.2' })
  $('#articleEditDiv').animate({ opacity: '1' })
}

function hide_all_converter_preview_tabs () {
  $('#EditHTML').hide()
  $('#EditLatex').hide()
  $('#PreviewLogs').hide()
}
function remove_all_converter_link_active () {
  $('#AEditHTML').removeClass('active')
  $('#AEditLatex').removeClass('active')
  $('#APreviewLogs').removeClass('active')
}

function show_converter_preview_tab (tab) {
  $('#' + tab).show()
}

function toggle_converter_tab (tab) {
  preview_loader()
  if (tab == 'EditLatex') {
    edit_latex_tabhelper()
  }
  if (tab == 'EditHTML') {
    edit_html_tabhelper()
  }
  if (tab == 'PreviewLogs') {
    logs_tabhelper()
  }
  hide_all_converter_preview_tabs()
  show_converter_preview_tab(tab)
  remove_all_converter_link_active()
  $('#A' + tab).addClass('active')
}

function edit_latex_tabhelper () {
  var latex = ''
  var type = ''

  if (addPage) {
    latex = $('#artilce_latex_dir').val()
    type = 'file'
  } else {
    latex = article_id
    type = 'db'
  }

  $.post(
    '/admin/articles/convert/latex/content/' + article_type,
    {
      latex: latex,
      type: type
    },
    function (data, status) {
      $('#latex_file_editor').val(data)
      $('#article_latex_content').val(data)
    }
  )
}

var editLatexStatus = false
$('#latex_file_editor').on('change keyup paste', function () {
  editLatexStatus = true
})

function edit_html_tabhelper () {
  if (editLatexStatus) {
    editLatexStatus = false
    write_updated_latex()
    convert_latex_to_html_api()
  }
}
function logs_tabhelper () {
  if (editLatexStatus) {
    write_updated_latex()
    convert_latex_to_html_api()
  }
}

var str_mgr = ''
var str_mgr_sp = []
var is_mgr_populated = false
var all_images_count = 0
var final_img_replaceto_img = []
var final_img_replace_img = []
var edit_image_mgr = false

function populate_image_manager (data) {
  final_img_replaceto_img = []
  final_img_replace_img = []
  $(' .modal-body > .ajax-file-upload-container').html('')
  $('#modal-table').html('')
  $('#modal-table').append(
    `<tr id = "modal-table-tr" ><td>Select Image</td><td>Caption</td><td>Action</td></tr>`
  )
  all_images_count = data.length
  for (var i = 0; i < all_images_count; i++) {
    str_mgr = data[i]
    str_mgr_sp = str_mgr.split('*res*')
    $('#modal-table').append(
      '<tr id="modal-table-tr"><td><img class="modal-push-image" src="" id="upx' +
        i +
        'si" style="display:none"><select class="upx_name_c" id="upx' +
        i +
        's"></select></td><td>' +
        "<input type='hidden' id='upx" +
        i +
        "c' value='" +
        str_mgr_sp[0] +
        "'>" +
        str_mgr_sp[1] +
        '</td>' +
        '<td><button class="imgmgr-save-btn" onclick="manage_image_link(this,\'upx' +
        i +
        "s','upx" +
        i +
        'c\');">save</button></td>' +
        '</tr >'
    )
  }
}
var img_url_prefix = '/articles/image/' + article_type + '/'

function manage_image_link (obj, l1, l2) {
  l1data = $('#' + l1).val()
  l2data = $('#' + l2).val()
  if (l1data == null || l2data == '') {
    alert('please select an image.')
    return 1
  }

  obj.style.backgroundColor = '#989898'
  obj.innerHTML = 'saved'
  final_img_replaceto_img.push(l1data)
  final_img_replace_img.push(l2data)
  $('#' + l1 + 'i').attr('src', img_url_prefix + $('#' + l1).val())
  $('#' + l1 + 'i').show()
}

function convert_imgmgr_results () {
  var d = new Date()
  if (
    !(final_img_replaceto_img.length == final_img_replace_img.length) ||
    (!(final_img_replace_img.length == all_images_count) &&
      !edit_image_mgr &&
      addPage)
  ) {
    alert('Please save all the images!')
  }
  var edi_dump = $('#latex_file_editor').val()
  for (var i = 0; i < final_img_replaceto_img.length; i++) {
    edi_dump = edi_dump.replace(
      final_img_replace_img[i],
      img_url_prefix + final_img_replaceto_img[i]
    )
  }
  $('#latex_file_editor').val(edi_dump)

  write_updated_latex()
  $('#manager_modal').modal('toggle')

  var date_stamp = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()
  conversion_success_notify('-Images were updated at ' + date_stamp)
  $('#PreviewLogs').append(
    '<p class="logs-p">Images were updated at ' + date_stamp + '</p><br>'
  )
  edit_image_mgr = true
}

var identified_cits = []
function populate_iden_cits () {
  $('#bm_1').html('')
  for (var i = 0; i < identified_cits.length; i++) {
    $('#bm_1').append('<tr><td>' + identified_cits[i] + '</td></tr>')
  }
}

function convert_latex_to_html_api () {
  var d = new Date()
  var latex = ''
  var type = ''
  if (addPage) {
    latex = $('#artilce_latex_dir').val()
    type = 'file'
  } else {
    latex = article_id
    type = 'db'
  }
  preview_loader()
  $.post(
    '/admin/articles/convert/latex/html/' + article_type,
    {
      latex: latex,
      type: type
    },
    function (data, status) {
      identified_cits = data.bib_manager

      populate_image_manager(data.image_manager)
      populate_iden_cits()
      CKEDITOR.instances.article_editor.setData(data.result)
    }
  )

  var date_stamp = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()
  conversion_success_notify('Latex file was converted to HTML at ' + date_stamp)
}

function write_updated_latex () {
  var url = ''
  var latex = ''
  var type = ''
  if (addPage) {
    latex = $('#artilce_latex_dir').val()
    type = 'file'
  } else {
    latex = article_id
    type = 'db'
  }
  var d = new Date()
  var initialCKdata = $('#latex_file_editor').val()
  if (initialCKdata !== '') {
    preview_loader()

    $.post(
      '/admin/articles/write/latex/content/' + article_type,
      {
        latex: latex,
        latex_content: initialCKdata,
        type: type
      },
      function (data, status) {
        convert_latex_to_html_api()
      }
    )
    var date_stamp = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()
    preview_success_notify('Latex file was updated at ' + date_stamp)
  }
}

function preview_success_notify (msg) {
  $('#preview_success_msg').html(msg)
  $('#preview_success_notify').show()
}

function conversion_success_notify (msg) {
  $('#conversion_success_msg').html(msg)
  $('#conversion_success_notify').show()
}

var tipInc = 0
var tips_array = [
  'Refresh the converter to re-convert the article.',
  'All the details are auto-saved.',
  'Please check logs for conversion details.',
  'You can edit the latex file in Edit latex tab.'
]
function show_converter_tips () {
  $('#converter_tip_msg').html(tips_array[tipInc % tips_array.length])
  tipInc++
}
setInterval(function () {
  show_converter_tips()
}, 3000)

function toggle_article_preview (toggle) {
  if (toggle == 'show') {
    var title = $('input[name="cdln_title"]').val()
    var authors = $('input[name="cdln_authors"]').val()
    var convertedHTML = CKEDITOR.instances.article_editor.getData()
    // authors = authors.split(',')
    // $('#pArticleAuthors').html('')
    // for (var j = 0; j < authors.length; j++) {
    //   if (j == authors.length - 1) {
    //     $('#pArticleAuthors').append(authors[j])
    //   } else {
    //     $('#pArticleAuthors').append(authors[j] + ', ')
    //   }
    // }
    $('#pArticleName').html(title)
    renderEditorMath.update(convertedHTML)
    $('#articlePreviewDiv').show()
    $('#article-form').hide()
    $('#relatedActions').hide()
    $('#articleEditDiv')
      .removeClass('col-lg-7')
      .addClass('col-lg-2')
    $('#closePreviewButton').show()
    preview_loader()
  } else {
    $('#articlePreviewDiv').hide()
    $('#article-form').show()
    $('#relatedActions').show()
    $('#articleEditDiv')
      .removeClass('col-lg-2')
      .addClass('col-lg-7')
    $('#closePreviewButton').hide()
  }
  $('#articlePreviewDiv').animate({ opacity: '.2' })
  $('#articlePreviewDiv').animate({ opacity: '1' })
  $('#articleEditDiv').animate({ opacity: '.2' })
  $('#articleEditDiv').animate({ opacity: '1' })
}

function re_convert_article () {
  editLatexStatus = true
  write_updated_latex()
  convert_latex_to_html_api()
}

$('#uploadImages').click(function () {
  $('#articleImage').click()
})

function process_cit (cite, brace, ob, main) {
  var edi_dump = $('#latex_file_editor').val()

  var cit_convertd = ''
  var added_o = ''
  var pub = ''
  if (ob.publisher) {
    pub = ', ' + ob.publisher
  }
  if (ob.volume) {
    added_o = added_o + ', In: JCS ' + ob.volume + ' '
  }
  if (ob.pages) {
    added_o = added_o + ', pp. ' + ob.pages + ' '
  }
  if (brace) {
    brace = ', ' + brace
  } else {
    brace = ''
  }
  if (cite == 'parencite') {
    cit_convertd =
      '(' + ob.author.replace('.', '') + ', ' + ob.year + ' ' + brace + ')'
    $('#bm_2x').append(
      '<tr><td>' +
        main +
        '</td><td style="width:60%;">' +
        cit_convertd +
        '.</td></tr>'
    )
  } else if (cite == 'nocite') {
    cit_convertd =
      ob.author.replace('.', '') +
      ', ' +
      ob.year +
      ', ' +
      '"' +
      ob.title +
      '"' +
      '<i>' +
      pub +
      '</i>' +
      added_o +
      brace +
      '.<br>'

    $('#bm_2x').append(
      '<tr><td>' +
        main +
        '</td><td style="width:60%;">' +
        cit_convertd +
        '</td></tr>'
    )
  } else {
    cit_convertd =
      ob.author.replace('.', '') + ', ' + ob.year + ' ' + ' ' + brace

    $('#bm_2x').append(
      '<tr><td>' +
        main +
        '</td><td style="width:60%;">' +
        cit_convertd +
        '.</td></tr>'
    )
  }

  edi_dump = edi_dump.replace(main, cit_convertd)
  $('#latex_file_editor').val(edi_dump)
}

function bib_manager_mapper (data) {
  $('.bm_wr').hide()
  $('#bm_2x').show()
  var id_str = ''
  var key = ''
  var brace = ''
  $('bm_2').html('')

  for (var i = 0; i < identified_cits.length; i++) {
    id_str = identified_cits[i]
    id_str_tmp = id_str
    if (id_str !== null) {
      key = id_str.match(/{.*}/)

      id_str = id_str.replace(key[0], '')

      key[0] = key[0].replace('{', '').replace('}', '')

      brace = id_str.match(/\[.*\]/)
      id_str = id_str.replace(brace, '')
      if (brace !== null) {
        brace = brace[0].replace('[', '').replace(']', '')
      }

      id_str = id_str.replace('\\', '')
      process_cit(
        id_str,
        brace,
        data.entries[data.mapped_bits[key]],
        id_str_tmp
      )
    }
  }

  editLatexStatus = true
  write_updated_latex()
}

var article_id = 0
var pub_id = 0
var article_title = ''
var pub_title = ''

function view_link () {
  $('#success_link').hide()
  $('#success_unlink').hide()
  $('#link_status').show()
  $('#link_status').html('')
  $('#link_status').append(
    '<tr><th>Article</th><th> Publication</th><th>Status</th><th>Action</th></tr>'
  )
  if (!article_id && !pub_id) {
    alert('Please select at least one article or publication')
  } else if (article_id && pub_id) {
    process_both()
  } else if (article_id) {
    process_article_links()
  } else if (pub_id) {
    process_pub_links()
  } else {
    alert('Process error')
  }
}

function process_pub_links () {
  $.get('/admin/api/view/links/' + pub_id + '/all/pubs', function (
    data,
    status
  ) {
    if (data.status == 'norecord') {
      $('#link_status').show()
      $('#link_status').append(
        '<tr><td class="ls_td1">' +
          pub_title +
          '</td><td class="ls_td2"></td><td class="ls_td2"> Not Linked to any articles</td><td class="ls_td2">-</tr>'
      )
    } else {
      $('#link_status').show()
      for (var i = 0; i < data.length; i++) {
        $('#link_status').append(
          '<tr><td class="ls_td1">' +
            data[i].title +
            '</td><td class="ls_td2">' +
            data[i].bibtexkey +
            '</td><td class="ls_td2"> Linked </td><td class="ls_td2"><button onclick=\'unlink("' +
            data[i].aid +
            '","' +
            data[i].pid +
            '")\'>Unlink</button></td></tr>'
        )
      }
    }
  })
}

function process_article_links () {
  $.get('/admin/api/view/links/' + article_id + '/all/article', function (
    data,
    status
  ) {
    if (data.status == 'norecord') {
      $('#link_status').show()
      $('#link_status').append(
        '<tr><td class="ls_td1">' +
          article_title +
          '</td><td class="ls_td2"></td><td class="ls_td2"> Not Linked to any publication</td><td class="ls_td2">-</tr>'
      )
    } else {
      $('#link_status').show()
      for (var i = 0; i < data.length; i++) {
        $('#link_status').append(
          '<tr><td class="ls_td1">' +
            article_title +
            '</td><td class="ls_td2">' +
            data[i].bibtexkey +
            '</td><td class="ls_td2"> Linked </td><td class="ls_td2"><button onclick=\'unlink("' +
            article_id +
            '","' +
            data[i].pid +
            '")\'>Unlink</button></td></tr>'
        )
      }
    }
  })
}

function process_both () {
  $.get('/admin/api/view/link/' + article_id + '/' + pub_id, function (
    data,
    status
  ) {
    if (data.status == 'norecord') {
      $('#link_status').show()
      $('#link_status').append(
        '<tr><td class="ls_td1">' +
          article_title +
          '</td><td class="ls_td2">' +
          pub_title +
          '</td><td class="ls_td2"> Not Linked </td><td class="ls_td2"><button onclick="link()">Link</button></td></tr>'
      )
    } else if (data[0].status == 0) {
      $('#link_status').show()
      $('#link_status').append(
        '<tr><td class="ls_td1">' +
          article_title +
          '</td><td class="ls_td2">' +
          pub_title +
          '</td><td class="ls_td2"> Un-linked </td><td class="ls_td2"><button onclick="link()">Link</button></td></tr>'
      )
    } else {
      $('#link_status').show()
      $('#link_status').append(
        '<tr><td class="ls_td1">' +
          article_title +
          '</td><td class="ls_td2">' +
          pub_title +
          '</td><td class="ls_td2"> Linked </td><td class="ls_td2"><button onclick=\'unlink("' +
          article_id +
          '","' +
          pub_id +
          '")\'>Unlink</button></td></tr>'
      )
    }
  })
}

function link () {
  $.get('/admin/api/link/' + article_id + '/' + pub_id, function (
    data,
    status
  ) {
    $('#link_status').hide()
    $('#success_link').show()
  })
}
function unlink (a, p) {
  $.get('/admin/api/unlink/' + a + '/' + p, function (data, status) {
    $('#link_status').hide()
    $('#success_unlink').show()
  })
}
$('#pubs').on('change', function () {
  $('#count-pub-results').hide()
  var key = $('#pubs').val()
  if (key == '' || key == ' ') {
    pub_id = 0
    manage_button_title()
    return
  } else if (isNaN(key)) {
    $('#count-pub-results').html('Unable to find <b>' + key + '</b>')
    $('#count-pub-results').show()
    return
  } else {
    $.get('/admin/api/get/' + key + '/pubs', function (data, status) {
      pub_id = data.id
      pub_title = data.bibtexkey
      $('#pubs').val(pub_title)
      $('#count-pub-results').html(
        'Id of the publication is <b>' + pub_id + '</b>'
      )
      $('#count-pub-results').show()
      manage_button_title()
    })
  }
})

$('#article').on('change', function () {
  $('#count-article-results').hide()
  var key = $('#article').val()
  if (key == '' || key == ' ') {
    article_id = 0
    manage_button_title()
    return
  } else if (isNaN(key)) {
    $('#count-article-results').html('Unable to find <b>' + key + '</b>')
    $('#count-article-results').show()
    return
  } else {
    $.get('/admin/api/get/' + key + '/article', function (data, status) {
      article_id = data.id
      article_title = data.title
      $('#article').val(article_title)
      $('#count-article-results').html(
        'Id of the article is <b>' + article_id + '</b>'
      )
      $('#count-article-results').show()
      manage_button_title()
    })
  }
})

function manage_button_title () {
  if (article_id && pub_id) {
    $('#view_button').html('View links')
  } else if (article_id) {
    $('#view_button').html('View Article links')
  } else {
    $('#view_button').html('View Publication links')
  }
}

$('#pubs').on('input', function () {
  var key = $('#pubs').val()
  if (key == '' || key == ' ' || key.length < 3) {
    return
  }
  $.get('/admin/api/suggest/' + key + '/publications', function (data, status) {
    $('#pubs_suggestions').html('')

    if (data.length < 1 && isNaN(key)) {
      $('#count-pub-results').show()
      $('#count-pub-results').html('No results found for <b>' + key + '</b>')
    } else {
      for (var i = 0; i < data.length; i++) {
        $('#pubs_suggestions').append(
          "<option value='" +
            data[i].id +
            "'>" +
            data[i].bibtexkey +
            '</option>'
        )
      }
    }
  })
})

$('#article').on('input', function () {
  var key = $('#article').val()
  if (key == '' || key == ' ' || key.length < 3) {
    return
  }
  $.get('/admin/api/suggest/' + key + '/articles', function (data, status) {
    $('#articles_suggestions').html('')

    if (data.length < 1 && isNaN(key)) {
      $('#count-article-results').show()
      $('#count-article-results').html(
        'No results found for <b>' + key + '</b>'
      )
    } else {
      for (var i = 0; i < data.length; i++) {
        $('#articles_suggestions').append(
          "<option onclick='alert(1)' value='" +
            data[i].id +
            "'>" +
            data[i].title +
            '</option>'
        )
      }
    }
  })
})
